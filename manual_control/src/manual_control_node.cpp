#include <ros/ros.h>
#include <ros/console.h>
#include "std_msgs/String.h"
#include <geometry_msgs/TwistStamped.h>
#include <sstream>
#include <termios.h>

int getch()
{
  static struct termios oldt, newt;
  tcgetattr( STDIN_FILENO, &oldt);           // save old settings
  newt = oldt;
  newt.c_lflag &= ~(ICANON);                 // disable buffering      
  tcsetattr( STDIN_FILENO, TCSANOW, &newt);  // apply new settings

  int c = getchar();  // read character (non-blocking)

  tcsetattr( STDIN_FILENO, TCSANOW, &oldt);  // restore old settings
  return c;
}

/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{

  ros::init(argc, argv, "manual_control_node");

  ros::NodeHandle n;

  ROS_INFO("Manual control node started");

  ros::Publisher start_stop_pub = n.advertise<std_msgs::String>("/manual_mode_topic", 1);
  ros::Publisher velocity_pub = n.advertise<geometry_msgs::TwistStamped>("/manual_mode_vel_topic", 10);

  //ros::Rate loop_rate(10);

  geometry_msgs::TwistStamped vel_msg;

  std::stringstream ss;
  std_msgs::String msg;
  int c;
  ROS_INFO("Press 'e' to enable and 'q' to quit publishing");
  ROS_INFO("Use 'w' 's' 'a' 'd' to control velocity");

  double linear = 0.0;
  double angular = 0.0;


  while (ros::ok())
  {

    c = getch();    

    if(c == 'e')
    {
      ROS_INFO("Enable keyboard control");
      ss << "Manual_Mode_Start";
      msg.data = ss.str();
      start_stop_pub.publish(msg);
      ss.str(std::string());
    }
    else if(c == 'q')
    {
      ROS_INFO("Quit keyboard control");
      ss << "Manual_Mode_Stop";
      msg.data = ss.str();
      start_stop_pub.publish(msg);
      ss.str(std::string());
    }
    else if(c == 'w')
    {
      linear += 0.1;
    }
    else if(c == 's')
    {
      linear -= 0.1;
    }
    else if(c == 'd')
    {
      angular -= 0.1;
    }

    else if(c == 'a')
    {
      angular += 0.1;
    }

    ROS_INFO("Velocity: %f, %f", linear, angular);

    vel_msg.twist.linear.x = linear;
    vel_msg.twist.angular.z = angular;
    vel_msg.header.stamp = ros::Time::now();
    velocity_pub.publish(vel_msg);


    ros::spinOnce();

    //loop_rate.sleep();
  }
  return 0;
}
