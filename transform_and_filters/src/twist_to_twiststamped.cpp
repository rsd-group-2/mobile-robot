#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TwistStamped.h>
#include <iostream>

class TwistToTwisted 
{
private:
	// Publisher and subscribers
	ros::Subscriber _sub;
	ros::Publisher _pub;
	ros::NodeHandle _n;

	void twistToStampedCallBack(const geometry_msgs::Twist::ConstPtr& twist);

public:
	// Default constructor
	TwistToTwisted();

	// Destructor
	~TwistToTwisted();
};

TwistToTwisted::TwistToTwisted()
{
	_sub = _n.subscribe<geometry_msgs::Twist>("/cmd_vel", 100, &TwistToTwisted::twistToStampedCallBack, this);
	// Publish the velocity to the main control unit: 
	//_pub = _n.advertise<geometry_msgs::TwistStamped>("/fmCommand/cmd_vel", 10);
	_pub = _n.advertise<geometry_msgs::TwistStamped>("/waypoint_slam_navigation_vel_topic", 10);
}

TwistToTwisted::~TwistToTwisted()
{

}


void TwistToTwisted::twistToStampedCallBack(const geometry_msgs::Twist::ConstPtr& twist)
{
	geometry_msgs::TwistStamped base_velocity;
	base_velocity.twist = *twist;

	// Get the header timings
	base_velocity.header.stamp = ros::Time::now();

	std::cout << "Velocities: " << base_velocity.twist.linear.x << ", " << base_velocity.twist.linear.y << std::endl;

	_pub.publish(base_velocity);
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "twist_to_stamped");

	TwistToTwisted twistedToTwisted;

	ROS_INFO("Twist to TwistStamped started");

	ros::spin();

	return 0;
}
