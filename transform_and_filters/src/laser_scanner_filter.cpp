#include <ros/ros.h>
#include "sensor_msgs/LaserScan.h"
#include <iostream>

const std::pair<int,int> LOWER_PILLAR_BOUND(0, 65); // 4 -27
const std::pair<int,int> UPPER_PILLAR_BOUND(215, 271); // 238 - 264

const float MAX_LASER_DIST = 4.0;

class LidarSensor
{
public:
	LidarSensor();

	~LidarSensor() {};

private:
	ros::Subscriber sub_;
	ros::Publisher pub_;
	ros::NodeHandle n_;

	sensor_msgs::LaserScan filtered_scan;

	void lidarParamReadingCallback(const sensor_msgs::LaserScan::ConstPtr& scan);
};

LidarSensor::LidarSensor()
{
	pub_ = n_.advertise<sensor_msgs::LaserScan>("/scan_filtered", 10);

	sub_ = n_.subscribe<sensor_msgs::LaserScan>("/scan", 10, &LidarSensor::lidarParamReadingCallback, this);
}

void LidarSensor::lidarParamReadingCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
	filtered_scan = *scan;
	//filtered_scan.header.stamp = ros::Time::now();

	int count = 0;
	for(size_t i = 0; i < scan->ranges.size(); i++)
	{
		filtered_scan.ranges[i] = scan->ranges[i];
		
		if((i >= LOWER_PILLAR_BOUND.first && i <= LOWER_PILLAR_BOUND.second) || (i >= UPPER_PILLAR_BOUND.first && i <= UPPER_PILLAR_BOUND.second))
			filtered_scan.ranges[i] = MAX_LASER_DIST;
	}
	
	pub_.publish(filtered_scan);
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "laser_scanner_filter");
	LidarSensor lidarSensor;

	ros::spin();
	return 1;
}
