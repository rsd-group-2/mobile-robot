#include "navigate_base.hpp"

NavigateBase::NavigateBase()
{
    m_vel_pub = m_nh.advertise<geometry_msgs::TwistStamped>(DEFAULT_VEL_PUB_TOPIC.c_str(), DEFAULT_NUM_MESSAGES);
    //m_pose_sub = m_nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>(DEFAULT_AMCL_POSE_SUB_TOPIC.c_str(), DEFAULT_NUM_MESSAGES, &NavigateBase::amclPoseCallback, this);
}

NavigateBase::NavigateBase(std::string publisher, std::string subscriber)
{
    m_vel_pub = m_nh.advertise<geometry_msgs::TwistStamped>(publisher.c_str(), DEFAULT_NUM_MESSAGES);
   // m_pose_sub = m_nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>(subscriber.c_str(), DEFAULT_NUM_MESSAGES, &NavigateBase::amclPoseCallback, this);
}

NavigateBase::~NavigateBase()
{

}

// Callback for the AMCL pose
/*void NavigateBase::amclPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& pose)
{
    // Lock the mutex
    std::lock_guard<std::mutex> lock(m_mutex);
    m_pose = *pose;
}*/

// Move the base forward
void NavigateBase::moveForward(float x_vel)
{
    if(x_vel > MAX_X_VEL)
        x_vel = MAX_X_VEL;

    m_twist.header.stamp = ros::Time::now();

    m_twist.twist.linear.x = x_vel;
    m_twist.twist.linear.y = 0;
    m_twist.twist.angular.z = 0;

    m_vel_pub.publish(m_twist);
}

// Move the base backwards
void NavigateBase::moveBackwards(float x_vel)
{
    if(x_vel > MAX_X_VEL)
        x_vel = MAX_X_VEL;

    m_twist.header.stamp = ros::Time::now();

    m_twist.twist.linear.x = -x_vel;
    m_twist.twist.linear.y = 0;
    m_twist.twist.angular.z = 0;

    m_vel_pub.publish(m_twist);
}

// Turn the base clockwise
void NavigateBase::turnRight(float theta_vel)
{
    if(theta_vel > MAX_THETA_VEL)
        theta_vel = MAX_THETA_VEL;

    m_twist.header.stamp = ros::Time::now();

    m_twist.twist.linear.x = 0;
    m_twist.twist.linear.y = 0;
    m_twist.twist.angular.z = -theta_vel;

    m_vel_pub.publish(m_twist);
}

// Turn the base counterclockwise
void NavigateBase::turnLeft(float theta_vel)
{
    if(theta_vel > MAX_THETA_VEL)
        theta_vel = MAX_THETA_VEL;

    m_twist.header.stamp = ros::Time::now();

    m_twist.twist.linear.x = 0;
    m_twist.twist.linear.y = 0;
    m_twist.twist.angular.z = theta_vel;

    m_vel_pub.publish(m_twist);
}

// Move the base in a given direction and heading
void NavigateBase::moveDirection(float x, float y, float theta)
{
    // Do nothing
}

// Stop the movement of the frobit robot
void NavigateBase::stopMovement()
{
    m_twist.header.stamp = ros::Time::now();

    m_twist.twist.linear.x = 0;
    m_twist.twist.linear.y = 0;
    m_twist.twist.angular.z = 0;

    m_vel_pub.publish(m_twist);
}

// Set the publisher topic
void NavigateBase::setPublisherTopic(const std::string publisher_topic)
{
    m_vel_pub = m_nh.advertise<geometry_msgs::TwistStamped>(publisher_topic.c_str(), DEFAULT_NUM_MESSAGES);
}

// Set the subscriber topic
void NavigateBase::setSubscriberTopic(const std::string subscriber_topic)
{
    //m_pose_sub = m_nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>(subscriber_topic.c_str(), DEFAULT_NUM_MESSAGES, &NavigateBase::amclPoseCallback, this);
}
