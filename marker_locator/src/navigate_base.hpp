#ifndef _NAVIGATE_BASE_H_
#define _NAVIGATE_BASE_H_
/***************************/
/* Base Class for a set of */
/* simple movements of the */
/* frobit robot            */
/***************************/
#include "ros/ros.h"

#include <iostream>
#include <string>
#include <mutex>

#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <std_msgs/String.h>

static const int DEFAULT_NUM_MESSAGES = 10;

static const float MAX_X_VEL = 1.0;
static const float MAX_THETA_VEL = 1.0;

static const std::string DEFAULT_VEL_PUB_TOPIC = "/waypoint_slam_navigation_vel_topic";
static const std::string DEFAULT_AMCL_POSE_SUB_TOPIC = "/amcl_pose";

class NavigateBase
{
    protected:
        geometry_msgs::TwistStamped m_twist;
        geometry_msgs::PoseWithCovarianceStamped m_pose;

    private:
        ros::NodeHandle m_nh;
        ros::Publisher m_vel_pub;
        //ros::Subscriber m_pose_sub;

        std::mutex m_mutex;
    public:

        NavigateBase();

        NavigateBase(std::string publisher, std::string subscriber);

        ~NavigateBase();

        // Callback for the AMCL pose
        //void amclPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &pose);

        // Move the base forward
        virtual void moveForward(float x_vel);

        // Move the base backwards
        virtual void moveBackwards(float x_vel);

        // Turn the base clockwise
        virtual void turnRight(float theta_vel);

        // Turn the base counterclockwise
        virtual void turnLeft(float theta_vel);

        // Move the base in a given direction and heading
        virtual void moveDirection(float x, float y, float theta);

	   // Stop the movement of the frobit robot
	   virtual void stopMovement();

        // Set the publisher topic
        void setPublisherTopic(const std::string publisher_topic);

        // Set the subscriber topic
        void setSubscriberTopic(const std::string subscriber_topic);

};

#endif /* _NAVIGATE_BASE_H_ */
