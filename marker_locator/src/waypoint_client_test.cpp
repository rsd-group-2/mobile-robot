#include "ros/ros.h"
#include "marker_locator/MarkerLocatorData.h"
#include "waypoint_navigator.hpp"
#include "std_msgs/String.h"
#include <string>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <cstdlib>
#include <mutex>

using namespace WayPointNav;

std::string active_state = "";
std::mutex mutex;
std_msgs::String new_state_str;

WayPoint<double> transformMarkerToMap(WayPoint<double> marker_locator_pos, geometry_msgs::PoseStamped& map_locator_pose)
{
    tf::TransformListener listener_;

    geometry_msgs::PoseStamped marker_locator_pose;

    marker_locator_pose.header.stamp = ros::Time::now();
    marker_locator_pose.header.frame_id = "marker_link";
    marker_locator_pose.pose.position.x = marker_locator_pos.x;
    marker_locator_pose.pose.position.y = marker_locator_pos.y;
    marker_locator_pose.pose.position.z = 0;
    tf::Quaternion q = tf::createQuaternionFromRPY(0,0,marker_locator_pos.theta + MARKER_THETA_OFFSET);
    marker_locator_pose.pose.orientation.x = q.getX();
    marker_locator_pose.pose.orientation.y = q.getY();
    marker_locator_pose.pose.orientation.z = q.getZ();
    marker_locator_pose.pose.orientation.w = q.getW();

    try
    {
        listener_.transformPose("map", marker_locator_pose, map_locator_pose);
        //return TRANSFORM_SUCCESSFUL;

    } catch (tf::TransformException& e)
    {
        ROS_ERROR("Error in trasnform: %s", e.what());
        //return TRANSFORM_UNSUCCESSFUL;
    }

}

geometry_msgs::PoseWithCovarianceStamped setInitialPose()
{
    geometry_msgs::PoseWithCovarianceStamped initial_pose;

    // Header and frame
    initial_pose.header.stamp = ros::Time::now();
    initial_pose.header.frame_id = "map";

    MarkerLocator marker_locator_obj_;

    marker_locator::marker_locator_msg marker_locator_msg;

    if(marker_locator_obj_.Connect() != SocketConnection::CONNECTION_SUCCEDED)
    {
        ROS_ERROR("Can't connect to the marker locator!");
    } else
    {
        marker_locator_msg = marker_locator_obj_.getMarkerLocatorData(false);
    }

    WayPoint<double> marker_locator_wpt(marker_locator_msg.x, marker_locator_msg.y, marker_locator_msg.theta);
    geometry_msgs::PoseStamped map_locator_pose;
    transformMarkerToMap(marker_locator_wpt, map_locator_pose);

        initial_pose.pose.pose.position.x = map_locator_pose.pose.position.x;       
        initial_pose.pose.pose.position.y = map_locator_pose.pose.position.y;
        initial_pose.pose.pose.orientation.x = map_locator_pose.pose.orientation.x;
        initial_pose.pose.pose.orientation.y = map_locator_pose.pose.orientation.y;
        initial_pose.pose.pose.orientation.z = map_locator_pose.pose.orientation.z;// + QUATERNION_Z_OFFSET;
        initial_pose.pose.pose.orientation.w = map_locator_pose.pose.orientation.w;// + QUATERNION_W_OFFSET;
        initial_pose.pose.covariance[0] = COV_X;
        initial_pose.pose.covariance[7] = COV_Y;
        initial_pose.pose.covariance[35] = COV_THETA;
        //pub_.publish(initial_pose);

    return initial_pose;
}

void stateChangeCallback(const std_msgs::String::ConstPtr& msg)
{
    std::lock_guard<std::mutex> lock(mutex);
    std::cout << "Overwriting new active state" << std::endl;
    active_state = msg->data.c_str();
    mutex.unlock();
}

int main (int argc, char** argv)
{
    ros::init(argc, argv, "waypoint_client_test");

    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<marker_locator::MarkerLocatorData>("waypoint_update_service");

    // Topic publisher and subscriber
    ros::Subscriber state_change_sub = n.subscribe("/main_state_topic", 10, stateChangeCallback);
    ros::Publisher state_change_pub = n.advertise<std_msgs::String>("/main_state_topic", 10);
    ros::Publisher pub_ = n.advertise<geometry_msgs::PoseWithCovarianceStamped>("initialpose", 1);

    ros::Rate loop_rate(10);

    // WAypoint test
    WayPointNav::WPTStackResult wpt_stack_result;
    /*new_state_str.data = "Waypoint_SLAM_Navigation";
    state_change_pub.publish(new_state_str);
    ros::spinOnce();*/

    WptNav<double> wpt_nav;

    int counter = 0;

    tf::TransformListener listener;
    tf::StampedTransform transform_in_map;
    while(ros::ok())
    {
        counter++;

        if(counter == 20)
        {
            new_state_str.data = "Waypoint_SLAM_Navigation";
            state_change_pub.publish(new_state_str);
        }
        /** DEBUG START **/
        // Check if the waypoint navigation is active
        if(active_state == "Waypoint_SLAM_Navigation")
        {
            std::lock_guard<std::mutex> lock(mutex);
            WayPointNav::WayPoint<double> wpt;
            marker_locator::MarkerLocatorData srv;
            std::cout << "Enter request" << std::endl;
            std::cout << "0: Move to dispenser, 1: Move to start of line, 2: Get marker data, 3: to end of Box and to Line, 4: Move from Line to Box 6: Get coordinates in map frame" << std::endl;
            int request;
            std::cin >> request;
            if(request == 10)
                return 0;
            else if(request == 0)
                    wpt = WayPointNav::WayPoint<double>(-1.255, 0.499, 0);
            else if(request == 1)
                wpt = WayPointNav::WayPoint<double>(2.353, 2.12828, -0.455);

            srv.request.request = 0;
            std::cout << "trolo" << std::endl;
            if(client.call(srv))
            {
                std::cout << "In clienT" << std::endl;
                ROS_INFO("Client called");

                if(srv.response.error > 0)
                {
                    ROS_ERROR("Some error occured");
                }
                else
                {
                    ROS_INFO("Response from srv");
                    ROS_INFO("marker x: %f", (double) srv.response.marker_x);
                    ROS_INFO("marker y: %f,", (double) srv.response.marker_y);
                    ROS_INFO("marker theta: %f", (double) srv.response.marker_theta);

                    ROS_INFO("Setting initial pose!");
                    if(request != 6)
                    {
                        wpt_nav.setInitialPose();
                        std::cout << "Setting pose" << std::endl;
                    }

		            std::cout << "Request si: " << request << std::endl;
                    if(request == 3)
                    {	
    			        new_state_str.data = "Waypoint_SLAM_Navigation";
                	    state_change_pub.publish(new_state_str);
    			        /*if(wptNav.moveFromBoxToLine() == WayPointNav::WAYPOINT_SUCCESSFULLY_REACHED)
    			        {
    		            	// Set next state to line follower
                            //wptNav.correctLinePosition();
    		            	new_state_str.data = "Line_Navigation";
    		            	state_change_pub.publish(new_state_str);
                        }*/
                    }    
		           if (request == 6)
                   {
                       // wptNav.correctLinePosition();
                        try
                        {
                            listener.waitForTransform("map", "base_link", ros::Time(0), ros::Duration(5));
                            listener.lookupTransform("map", "base_link", ros::Time(0), transform_in_map);
                            double roll, pitch, yaw;
                            tf::Matrix3x3 m(transform_in_map.getRotation());
                            m.getRPY(roll, pitch, yaw);
                            std::cout << "Robot x and y coordinates in the map is: " << transform_in_map.getOrigin().x() << " " << transform_in_map.getOrigin().y() << std::endl;
                            std::cout << "yaw: " << yaw << std::endl;
                            //ROS_INFO("x and y is: %d , %i", transform_in_map.getOrigin().x(), transform_in_map.getOrigin().y());

                        } catch (tf::TransformException &exception)
                        {
                            ROS_ERROR("%s", exception.what());
                        }
                    }

                }
            }
            else
            {
                ROS_ERROR("Failed to call service");
            }
            mutex.unlock();
        }
        ros::spinOnce();
        loop_rate.sleep();
    }
}

