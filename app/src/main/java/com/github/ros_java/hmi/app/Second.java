package com.github.ros_java.hmi.app;

import org.ros.android.RosActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.view.ViewGroup.MarginLayoutParams;

import org.ros.android.MessageCallable;
import org.ros.android.view.RosTextView;
import org.ros.address.InetAddressFactory;
import java.lang.String;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

public class Second extends RosActivity {

    private TextView state;
    private TextView position;
    private ImageView image;
    private ImageView image2;
    private MarginLayoutParams lp;
    private int image_width;
    private int image_height;
    private int image2_width;
    private int image2_height;
    private float ratio_width;
    private float ratio_height;
    private float new_width;
    private float new_height;
    private static float line_in_width = 332f;
    private static float line_in_height = 358f;
    private static float line_out_width = 360f;
    private static float line_out_height = 383f;
    private static float entrance_1_width = 58f;
    private static float entrance_1_height = 279f;
    private static float exit_1_width = 27f;
    private static float exit_1_height = 315f;
    private static float conveyor_1_width = 126f;
    private static float conveyor_1_height = 279f;
    private static float load_1_width = 154f;
    private static float load_1_height = 315f;
    private static float entrance_2_width = 58f;
    private static float entrance_2_height = 223f;
    private static float exit_2_width = 27f;
    private static float exit_2_height = 186f;
    private static float conveyor_2_width = 126f;
    private static float conveyor_2_height = 223f;
    private static float load_2_width = 154f;
    private static float load_2_height = 186f;
    private static float entrance_3_width = 58f;
    private static float entrance_3_height = 30f;
    private static float exit_3_width = 27f;
    private static float exit_3_height = 65f;
    private static float conveyor_3_width = 126f;
    private static float conveyor_3_height = 30f;
    private static float load_3_width = 154f;
    private static float load_3_height = 65f;
    private ToggleButton safety_button;
    private RosTextView<std_msgs.String> android_sub;
    private com.github.rosjava.android_package.android_pubsub.Talker android_pub = new com.github.rosjava.android_package.android_pubsub.Talker("/interaction_to_pc");

    private GestureDetector gestureDetector;
    public Intent nextScreen;

    public Second() {
        // The RosActivity constructor configures the notification title and ticker
        // messages.
        super("Second", "Second");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        state = (TextView) findViewById(R.id.statepage2);
        position = (TextView) findViewById(R.id.positionpage2);
        image = (ImageView) findViewById(R.id.imageView);
        image2 = (ImageView) findViewById(R.id.imageView2);
        lp = (MarginLayoutParams) image2.getLayoutParams();

        // Initial subscriber
        android_sub = (RosTextView<std_msgs.String>) findViewById(R.id.subscriber);
        android_sub.setTopicName("/interaction_to_android");
        android_sub.setMessageType(std_msgs.String._TYPE);
        android_sub.setMessageToStringCallable(new MessageCallable<String, std_msgs.String>() {
            @Override
            public java.lang.String call(std_msgs.String message) {

                String h = message.getData();
                String[] array = h.split(",");

                image_width = image.getWidth();
                image_height = image.getHeight();
                image2_width = image2.getWidth();
                image2_height = image2.getHeight();
                ratio_width = (float) image_width / (float) 512;
                ratio_height = (float) image_height / (float) 428;

                if (array[0] != null && array[3] != null) {
                    if (array[3].equals("Line")) {
                        if (array[0].equals("line_in")) {
                            new_width = line_in_width * ratio_width;
                            new_height = line_in_height * ratio_height;
                        } else if (array[0].equals("line_out")) {
                            new_width = line_out_width * ratio_width;
                            new_height = line_out_height * ratio_height;
                        } else if (array[0].equals("wc_1_entrance")) {
                            new_width = entrance_1_width * ratio_width;
                            new_height = entrance_1_height * ratio_height;
                        } else if (array[0].equals("wc_2_entrance")) {
                            new_width = entrance_2_width * ratio_width;
                            new_height = entrance_2_height * ratio_height;
                        } else if (array[0].equals("wc_3_entrance")) {
                            new_width = entrance_3_width * ratio_width;
                            new_height = entrance_3_height * ratio_height;
                        } else if (array[0].equals("wc_1_exit")) {
                            new_width = exit_1_width * ratio_width;
                            new_height = exit_1_height * ratio_height;
                        } else if (array[0].equals("wc_2_exit")) {
                            new_width = exit_2_width * ratio_width;
                            new_height = exit_2_height * ratio_height;
                        } else if (array[0].equals("wc_3_exit")) {
                            new_width = exit_3_width * ratio_width;
                            new_height = exit_3_height * ratio_height;
                        } else if (array[0].equals("wc_1_conveyor")) {
                            new_width = conveyor_1_width * ratio_width;
                            new_height = conveyor_1_height * ratio_height;
                        } else if (array[0].equals("wc_2_conveyor")) {
                            new_width = conveyor_2_width * ratio_width;
                            new_height = conveyor_2_height * ratio_height;
                        } else if (array[0].equals("wc_3_conveyor")) {
                            new_width = conveyor_3_width * ratio_width;
                            new_height = conveyor_3_height * ratio_height;
                        } else if (array[0].equals("wc_1_load")) {
                            new_width = load_1_width * ratio_width;
                            new_height = load_1_height * ratio_height;
                        } else if (array[0].equals("wc_2_load")) {
                            new_width = load_2_width * ratio_width;
                            new_height = load_2_height * ratio_height;
                        } else if (array[0].equals("wc_3_load")) {
                            new_width = load_3_width * ratio_width;
                            new_height = load_3_height * ratio_height;
                        }

                        state.setText(array[0]);
                        position.setText(array[3]);
                    } else if (array[0].equals("Waypoint_Navigation")) {

                        float marker_x = Float.parseFloat(array[3]);
                        float marker_y = Float.parseFloat(array[4]);

                        new_width = (245f + Math.round((marker_y * 100f) / 2.0f)) * ratio_width;
                        new_height = (223f + Math.round((marker_x * 100f) / 2.0f)) * ratio_height;

                        if (new_width > image_width || new_height > image_height || new_width < 0 || new_height < 0)
                        {
                            new_width = 0.0f * ratio_width;
                            new_height = 0.0f * ratio_height;

                            position.setText("Position not found");
                        }
                        else
                        {
                            position.setText(array[3] + ", " + array[4]);
                        }

                        state.setText(array[0]);

                    }
                    else if (array[0].equals("Idle"))
                    {
                        new_width = 245f * ratio_width;
                        new_height = 223f * ratio_height;

                        state.setText(array[0]);
                        position.setText("0, 0");
                    }
                    else {
                        new_width = 0.0f * ratio_width;
                        new_height = 0.0f * ratio_height;

                        state.setText(array[0]);
                        position.setText(array[3]);
                    }
                }

                lp.leftMargin = Math.round(new_width) - Math.round((float) (image2_width / 2));
                lp.topMargin = Math.round(new_height) - Math.round((float) (image2_height / 2));
                image2.setLayoutParams(lp);

                return null;
            }
        });

        gestureDetector = new GestureDetector(getApplicationContext(),new SwipeGestureDetector());
        nextScreen = new Intent(getApplicationContext(), App.class);

        Bundle extras = getIntent().getExtras();
        String control;
        String tipper;
        String safety;

        if (extras != null) {
            control = extras.getString("control2screen");
            tipper = extras.getString("tipper2screen");
            safety = extras.getString("safety2screen");
        }
        else {
            control = null;
            tipper = null;
            safety = null;
        }

        if (control != null) {
            if (control.equals("manual")) {
                nextScreen.putExtra("control1screen", "manual");
            }
            else if (control.equals("auto")) {
                nextScreen.putExtra("control1screen", "auto");
            }
        }
        else {
            nextScreen.putExtra("control1screen", "auto");
        }

        if (tipper != null) {
            if (tipper.equals("down")) {
                nextScreen.putExtra("tipper1screen", "down");
            }
            else if (tipper.equals("up")) {
                nextScreen.putExtra("tipper1screen", "up");
            }
        }
        else {
            nextScreen.putExtra("tipper1screen", "down");
        }

        safety_button = (ToggleButton) findViewById(R.id.safety_button);

        if (safety != null) {
            if (safety.equals("start")) {
                safety_button.setChecked(false);
                nextScreen.putExtra("safety1screen", "start");
            }
            else if (safety.equals("stop")) {
                safety_button.setChecked(true);
                nextScreen.putExtra("safety1screen", "stop");
            }
        }
        else {
            safety_button.setChecked(false);
            nextScreen.putExtra("safety1screen", "start");
        }
    }

    @Override
    protected void init(NodeMainExecutor nodeMainExecutor) {

        NodeConfiguration nodeConfiguration = NodeConfiguration.newPublic(InetAddressFactory.newNonLoopback().getHostAddress(),getMasterUri());

        // Execute publisher
        nodeMainExecutor.execute(android_pub, nodeConfiguration);

        // Execute subscriber
        nodeMainExecutor.execute(android_sub, nodeConfiguration);
    }

    //listener for the deadman state
    public void onToggleClicked(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            android_pub.set_safety("stop");
            nextScreen.putExtra("safety1screen", "stop");

            nextScreen.putExtra("control1screen", "manual");
        } else {
            android_pub.set_safety("start");
            nextScreen.putExtra("safety1screen", "start");

            nextScreen.putExtra("tipper1screen", "down");
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (gestureDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    private void onLeftSwipe() {
        // Do something
        startActivity(nextScreen);
    }

    // Private class for gestures
    private class SwipeGestureDetector
            extends GestureDetector.SimpleOnGestureListener {
        // Swipe properties, you can change it to make the swipe
        // longer or shorter and speed

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float velocityX, float velocityY) {
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onLeftSwipe();
                        }
                    }
                }

            } catch (Exception e) {
                Log.e("App", " Error on gestures");
            }
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_second, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
