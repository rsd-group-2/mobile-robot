package com.github.ros_java.hmi.app;

import android.content.Intent;
import android.os.Bundle;

import org.ros.android.MessageCallable;
import org.ros.android.RosActivity;
import org.ros.android.view.RosTextView;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

import org.ros.address.InetAddressFactory;
import java.lang.String;

import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;


public class App extends RosActivity
{

    private RosTextView<std_msgs.String> android_sub;
    private com.github.rosjava.android_package.android_pubsub.Talker android_pub = new com.github.rosjava.android_package.android_pubsub.Talker("/interaction_to_pc");

    private Button Nav_up;
    private Button Nav_down;
    private Button Nav_left;
    private Button Nav_right;
    private TextView state_text;
    private TextView tipper_text;
    private TextView status_text;
    private ToggleButton safety_button;
    private ToggleButton control_button;
    private ToggleButton tipper_button;

    private GestureDetector gestureDetector;
    public Intent nextScreen;

    public App() {
        // The RosActivity constructor configures the notification title and ticker
        // messages.
        super("App", "App");
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // Initial textviews for text from pc
        state_text = (TextView) findViewById(R.id.state2);
        tipper_text = (TextView) findViewById(R.id.tipper2);
        status_text = (TextView) findViewById(R.id.status2);


        // Initial subscriber
        android_sub = (RosTextView<std_msgs.String>) findViewById(R.id.subscriber);
        android_sub.setTopicName("/interaction_to_android");
        android_sub.setMessageType(std_msgs.String._TYPE);
        android_sub.setMessageToStringCallable(new MessageCallable<String, std_msgs.String>() {
            @Override
            public java.lang.String call(std_msgs.String message) {

                String h = message.getData();
                String[] array = h.split(",");

                if (array.length > 0) {
                    state_text.setText(array[0]);
                    tipper_text.setText(array[1]);
                    status_text.setText(array[2]);
                }

                return null;
            }
        });

        gestureDetector = new GestureDetector(getApplicationContext(),new SwipeGestureDetector());

        //Starting a new Intent
        nextScreen = new Intent(getApplicationContext(), Second.class);

        Bundle extras = getIntent().getExtras();
        String control;
        String tipper;
        String safety;

        if (extras != null) {
            control = extras.getString("control1screen");
            tipper = extras.getString("tipper1screen");
            safety = extras.getString("safety1screen");
        }
        else {
            control = null;
            tipper = null;
            safety = null;
        }

        control_button = (ToggleButton) findViewById(R.id.toggleButton1);

        if (control != null) {
            if (control.equals("manual")) {
                control_button.setChecked(false);
                nextScreen.putExtra("control2screen", "manual");
            }
            else if (control.equals("auto")) {
                control_button.setChecked(true);
                nextScreen.putExtra("control2screen", "auto");
            }
        }
        else {
            control_button.setChecked(true);
            nextScreen.putExtra("control2screen", "auto");
        }

        tipper_button = (ToggleButton) findViewById(R.id.toggleButton2);

        if (tipper != null) {
            if (tipper.equals("down")) {
                tipper_button.setChecked(false);
                nextScreen.putExtra("tipper2screen", "down");
            }
            else if (tipper.equals("up")) {
                tipper_button.setChecked(true);
                nextScreen.putExtra("tipper2screen", "up");
            }
        }
        else {
            tipper_button.setChecked(false);
            nextScreen.putExtra("tipper2screen", "down");
        }

        safety_button = (ToggleButton) findViewById(R.id.safety2);

        if (safety != null) {
            if (safety.equals("start")) {
                safety_button.setChecked(false);
                nextScreen.putExtra("safety2screen", "start");
            }
            else if (safety.equals("stop")) {
                safety_button.setChecked(true);
                nextScreen.putExtra("safety2screen", "stop");
            }
        }
        else {
            safety_button.setChecked(false);
            nextScreen.putExtra("safety2screen", "start");
        }

        addListenerOnButton();

    }

    @Override
    protected void init(NodeMainExecutor nodeMainExecutor) {

        NodeConfiguration nodeConfiguration = NodeConfiguration.newPublic(InetAddressFactory.newNonLoopback().getHostAddress(), getMasterUri());

        // Execute publisher
        nodeMainExecutor.execute(android_pub, nodeConfiguration);

        // Execute subscriber
        nodeMainExecutor.execute(android_sub, nodeConfiguration);
    }

    //Navigation controls
    public void addListenerOnButton() {

        Nav_up = (Button) findViewById(R.id.up);
        Nav_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android_pub.set_navigation("up");
            }

        });
        Nav_down = (Button) findViewById(R.id.down);
        Nav_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android_pub.set_navigation("down");
            }

        });
        Nav_left = (Button) findViewById(R.id.left);
        Nav_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android_pub.set_navigation("left");
            }

        });
        Nav_right = (Button) findViewById(R.id.right);
        Nav_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android_pub.set_navigation("right");
            }

        });
    }
    //listener for the control mode toggle button
    public void onToggleClicked(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            android_pub.set_Control_mode("auto");
            nextScreen.putExtra("control2screen", "auto");
        } else {
            android_pub.set_Control_mode("manual");
            nextScreen.putExtra("control2screen", "manual");
        }
    }

    //listener for the tipper toggle button
    public void onToggle2Clicked(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            android_pub.set_tipper("up");
            nextScreen.putExtra("tipper2screen", "up");
        } else {
            android_pub.set_tipper("down");
            nextScreen.putExtra("tipper2screen", "down");
        }
    }

    //listener for the deadman state
    public void onToggle3Clicked(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            android_pub.set_safety("stop");
            nextScreen.putExtra("safety2screen", "stop");

            control_button.setChecked(false);
            nextScreen.putExtra("control2screen", "manual");
        } else {
            android_pub.set_safety("start");
            nextScreen.putExtra("safety2screen", "start");

            tipper_button.setChecked(false);
            nextScreen.putExtra("tipper2screen", "down");
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (gestureDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }

    private void onRightSwipe() {
        // Do something
        startActivity(nextScreen);
    }

    // Private class for gestures
    private class SwipeGestureDetector
            extends GestureDetector.SimpleOnGestureListener {
        // Swipe properties, you can change it to make the swipe
        // longer or shorter and speed

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float velocityX, float velocityY) {
            try {
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX < 0) {
                            onRightSwipe();
                        }
                    }
                }

            } catch (Exception e) {
                Log.e("App", " Error on gestures");
            }
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_app, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
