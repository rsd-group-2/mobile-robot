#include "fsm_nav.h"

//NOTE :
//-add waiting for order to enable disable the navigation;
//-add error handling 
// Called once when the goal completes
void doneCb(const actionlib::SimpleClientGoalState& state,
            const move_base_msgs::MoveBaseResult::ConstPtr& result)
{
  ROS_INFO("Finished in state [%s]", state.toString().c_str());
  
  ros::shutdown();
}

// Called once when the goal becomes active
void activeCb()
{
  ROS_INFO("Goal just went active");

}


void feedbackCb(const move_base_msgs::MoveBaseFeedback::ConstPtr& feedback)
{
  
}

void navigation_fsm::lidarParamReadingCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{

	std::lock_guard	<std::mutex> lock(mutex_);
	laser_scanner_dist_ = scan->ranges[125];
}

void navigation_fsm::batteryCallback(const msgs::nmea::ConstPtr& bat)
{
double voltage = std::atoi(bat->data[3].c_str()) * 0.03747;
    if( voltage < 12.7)
		low_battery = true;
    else
		low_battery = false;

}
void navigation_fsm::lineFollowerCallback(const geometry_msgs::TwistStamped::ConstPtr& vel)
{
	line_vel_ = *vel;

	// Divde speed
	line_vel_.twist.linear.x = line_vel_.twist.linear.x/3;
	line_vel_.twist.angular.z = line_vel_.twist.angular.z;

	line_vel_.header.stamp = ros::Time::now();

	if(NAV_STATE == SLAMstate::DOCKING)
		_vel.publish(line_vel_);

}

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
//MoveBaseClient ac("move_base", true);

using namespace fsm_nav;
        
void navigation_fsm::poseReceivedCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{
		//std::lock_guard<std::mutex> lock(pose_mutex_);
		pos_= *msg;
		//std::cout <<"\tx:"<<pos_.pose.pose.position.x << "\ty:" << pos_.pose.pose.position.y << "\torientation:" << pos_.pose.pose.orientation.z << std::endl;
}

void navigation_fsm::robotStateCallback(const std_msgs::String::ConstPtr& str)
{
	// Check if the state has changed from line_follower to waypoint
	ROS_ERROR("Recieved string: %s", str->data.c_str());

	if (str->data.compare("Waypoint_Navigation") == 0 && (NAV_STATE == SLAMstate::LINE_FOLLOWER_ACTIVE || NAV_STATE == SLAMstate::LINE_FOLLOWER))
	{	
		ROS_ERROR("Change from line to waypoint");
		NAV_STATE = SLAMstate::LINE_FOLLOWER_BACK;
	} 
	// If the robot is in an idle state, start doing something
	else if (str->data.compare("Waypoint_Navigation") == 0 && NAV_STATE == SLAMstate::IDLE)
	{
		ROS_ERROR("state changed to charging");
		NAV_STATE = SLAMstate::CHARGING_STATION;
	}
	else if(str->data.compare("Idle") == 0 ){
		ROS_ERROR("Set to Idle");
		NAV_STATE = SLAMstate::IDLE;}
	// DEBUG
	//wpt_nav_.setInitialPose();
/*	else if (str->data.compare("Waypoint_Navigation") == 0 && NAV_STATE != SLAMstate::IDLE)
	{
		ROS_ERROR("state changed to LINE_FOLLOWER_BACK");
		NAV_STATE = SLAMstate::LINE_FOLLOWER_BACK;
	}*/
		
		robot_state=*str;
		ROS_ERROR("ROBOT STATE:[%s]",str->data.c_str());
		
}
//class constructor
navigation_fsm::navigation_fsm()
{
//		ros::init(0,"pik", "FSM_MACHINE");
		ROS_WARN("Waypoint fsm started");
		//real vel topic waypoint_slam_navigation_vel_topic
		_vel = nh.advertise<geometry_msgs::TwistStamped>("/waypoint_slam_navigation_vel_topic", 10);
	 	//_vel = nh.advertise<geometry_msgs::TwistStamped>("/fmCommand/cmd_vel",10);
	 	_pose = nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>("/amcl_pose",100,&navigation_fsm::poseReceivedCallback,this);
		pub_decider = nh.advertise<std_msgs::String>("/main_state_topic",10);
		_sub_decider = nh.subscribe<std_msgs::String>("/main_state_topic", 10, &navigation_fsm::robotStateCallback, this);
		clear_costmap_srv_ = nh.serviceClient<std_srvs::Empty>("/move_base/clear_costmaps");		
		// DEBUG
		_line_follower = nh.advertise<std_msgs::String>("/line_following_on_off_topic", 10);
		_laser_data = nh.subscribe<sensor_msgs::LaserScan>("/scan_filtered", 10, &navigation_fsm::lidarParamReadingCallback, this);
		_line_follower_speed = nh.subscribe<geometry_msgs::TwistStamped>("/line_follower_topic", 10, &navigation_fsm::lineFollowerCallback, this);
		_battery = nh.subscribe<msgs::nmea>("/fmSignal/nmea_from_frobit", 1, &navigation_fsm::batteryCallback,this);
		navigation_fsm::init();
}

void navigation_fsm::init()
{
		
		//temp
		NAV_STATE = SLAMstate::IDLE;// BOX_IN;

		//box_in_state_ = BoxInState::MOVE_BASE;
		//NAV_STATE = CHARGING_STATION;
		//NAV_STATE = DOCKING
		// NAV_STATE = LINE_FOLLOWER_BACK;
		//define zones in the map and assign the initial state depending on the localizatio
}
//wrapper of the state machine
void navigation_fsm::fsmWrapper()
{
	
	//if(strcmp(robot_state.data.c_str(),"order_received") || strcmp(robot_state.data.c_str(),"SLAM_Navigation") || strcmp(robot_state.data.c_str(),"Waypoint_Navigation"))
	//{
		STATE=ENABLED;
	//}
	//else
		//STATE=DISABLED;	
	
	if(STATE == ENABLED)
	{	

		switch(NAV_STATE)
		{
			case SLAMstate::IDLE:
			// Await next order.. Callback is used to set the state.
			ros::Duration(1.0).sleep();
			break;
			case(CHARGING_STATION):
			//switch to order received to slam navigation
			//navigation_fsm::switchArea();
			navigation_fsm::disengaging();
			break;
			case(DISENGAGED):
			navigation_fsm::navigateToDispenser();
			break;
			case(BRICK_DISPENSER):
			navigation_fsm::leaveBox();
			break;
			case(BOX_OUT):
			//switch to wpt navigation state
			//navigation_fsm::switchArea();	
			navigation_fsm::wptNavigation();
			break;
			case(LINE_FOLLOWER):
			main_state_str_.data = "Line_Navigation";
			pub_decider.publish(main_state_str_);
			NAV_STATE = SLAMstate::LINE_FOLLOWER_ACTIVE;
			ROS_ERROR("State is: LINE FOLLOWER ACTIVE");
			break;
			case LINE_FOLLOWER_ACTIVE:
			// Do nothing. Go masturbate or something.
			ros::Duration(1.0).sleep();
			break;
			case(LINE_FOLLOWER_BACK):
			navigation_fsm::wptnavigationBack();
			break;
			case(BOX_IN):
			//switch to slam navigation state
			//navigation_fsm::switchArea();
			navigation_fsm::approachingChargingStation();
			//navigation_fsm::docking();
			case(DOCKING):
			//switch to waiting for order state
			//navigation_fsm::switchArea()
			navigation_fsm::dockingTemp();
			break;
			case(ERROR):
			//define error solving function
			break;
		}
	}
}
//disengage the robot from the charging station
void navigation_fsm::disengaging(){
	
	//ROS_ERROR("DISENGAGING THE CHARGING STATION");
	geometry_msgs::TwistStamped twist;
	if(pos_.pose.pose.position.x > DISENGAGED_X_DES)
	{
		
		twist.header.stamp = ros::Time::now();
		twist.twist.linear.x = -0.2;
		
		_vel.publish(twist);
	}
	else
	{	
		twist.header.stamp = ros::Time::now();
		twist.twist.linear.x = 0.0;
		_vel.publish(twist);
		ros::Duration(4.0).sleep();
		if(low_battery){
			ROS_ERROR("LOW BATTERY");
			std_msgs::String line_follower_str;
			line_follower_str.data = "on";
        	_line_follower.publish(line_follower_str);
        	ros::Duration(2.0).sleep();
			//charging = true;
 			setSlamState(DOCKING);
			charging = true;
		}else
		setSlamState(DISENGAGED);
		
	
	}
}
//navigate to brick dispenser
void navigation_fsm::navigateToDispenser(){
	MoveBaseClient ac("move_base", true);
	
	while(!ac.waitForServer(ros::Duration(5.0))){
	
		ROS_INFO("Waiting for the move_base action server to come up");
  	}
	
	ROS_ERROR("APPROACHING THE LEGO DISPENSER");

	WayPointNav::WayPoint<double> wpt(-1.14, 0.55, M_PI);
	if(wpt_nav_.moveToWpt(wpt, true) == WayPointNav::WAYPOINT_SUCCESSFULLY_REACHED)
	{
		ros::Duration(10.0).sleep();
		setSlamState(BRICK_DISPENSER);
	}
	else
	{
		ros::Duration(10.0).sleep();
	}
	/*goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();
	goal.target_pose.pose.position.x = -1.07;
	goal.target_pose.pose.position.y = 0.55;
	goal.target_pose.pose.orientation= tf::createQuaternionMsgFromYaw(180*M_PI/180);*/
	//ac.sendGoal(goal);
	//ac.waitForResult();

	/*while(ac.getState() != actionlib::SimpleClientGoalState::SUCCEEDED)
	{
		//ROS_ERROR("State is: %i", ac.getState());
		ros::spinOnce();
		ros::Duration(0.1).sleep();
	}

	if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
		ROS_INFO("LEGO DISPENSER REACHED");
	else
		ROS_INFO("THE ROBOT HAS FAILED TO MOVE");*/

	//setSlamState(BRICK_DISPENSER);
}
//change goal coordinates with real one
void navigation_fsm::leaveBox(){
	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0))){
	
		ROS_INFO("Waiting for the move_base action server to come up");
  	}
	
	ROS_INFO("MOVING TO THE BOX EXIT");
	
	
	double x_step = 0.09;
	double x_des = -0.778;
	double y_des = 3.518;
	int i = 5;
	double x = x_des - x_step*i;
	double y =( 8.710 + 6.674* x);
	
	WayPointNav::WayPoint<double> wpt(-0.84,0.80 ,90*M_PI/180 );
	if(wpt_nav_.moveToWpt(wpt, true)  != WAYPOINT_SUCCESSFULLY_REACHED)
	{
		//WayPointNav::WayPoint<double> wpt_(-0.553016, 4.28983, 0);
                //if(wpt_nav_.moveToWpt(wpt_, true)  != WAYPOINT_SUCCESSFULLY_REACHED)
                //{  
                ros::Duration(3.0).sleep();
                //wpt_nav_.moveToWpt(wpt_,true);
                //}
                 //else
                //{
                //ROS_INFO("READY TO MOVE OUTSIDE");
                //setSlamState(BOX_OUT);
		//}
	}
	else
	{
	 //ros::Duration(4.0).sleep();
	        ROS_INFO("READY TO MOVE OUTSIDE");
                setSlamState(BOX_OUT);

	}
}

//wptnav to line MARK switch to LINE_FOLLOWER
void navigation_fsm::wptNavigation()
{
	ROS_ERROR("MOVING TO WAYPOINT");
	if (wpt_nav_.moveFromBoxToLine() == WayPointNav::WAYPOINT_SUCCESSFULLY_REACHED)
	{
		// Correct position relative to the line
		setSlamState(LINE_FOLLOWER);
		//setSlamState(LINE_FOLLOWER_BACK);
	} else
	{
		ROS_ERROR("ERROR FSM_NAV: Unable to reach waypoint");
	}
}
//wptnav from line MARK switch to BOX_IN in the endvoid 
void navigation_fsm::wptnavigationBack() 
{
	ROS_ERROR("MOVING TO BOX");
	// Clear the costmap
	//std_srvs::Empty srv;
	//if(!clear_costmap_srv_.call(srv))
		//ROS_ERROR("Error clearing the costmap");
	if (wpt_nav_.moveFromLineToBox() == WayPointNav::WAYPOINT_SUCCESSFULLY_REACHED)
	{
		// Correct position relative to the line
		ROS_ERROR("SWTICHING TO BOX IN");
		//setSlamState(BOX_IN);
	} 
	else
	{
		ROS_ERROR("ERROR FSM_NAV: Unable to reach waypoint");
	}
	charging == false;
	setSlamState(BOX_IN);
}

void navigation_fsm::approachingChargingStation(){
	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0))){
	
		ROS_INFO("Waiting for the move_base action server to come up");
  	}
  	//std_srvs::Empty srv;
	//wpt_nav_.setInitialPose();
	//clear_costmap_srv_.call(srv);
	//ros::Duration(3.0).sleep();
	ROS_INFO("PREPARING FOR DOCKING");
	//WayPointNav::WayPoint<double> wpt_(-1.04, 1.05 , -90*M_PI/180);
	//wpt_nav_.moveToWpt(wpt_,false);
	//ros::Duration(4.0).sleep();
	WayPointNav::WayPoint<double> wpt_(-1.04, 2.55, -90*M_PI/180+0.3);		
	WayPointNav::WayPoint<double> wpt(-1.04, 0.55, -90*M_PI/180+0.3);
	if(wpt_nav_.moveToWpt(wpt_, true) != WAYPOINT_SUCCESSFULLY_REACHED)
	{
		
		//wpt=WayPointNav::WayPoint<double>(0.201, 4.307, 0.0);
		//if(wpt_nav_.moveToWpt(wpt, true) != WAYPOINT_SUCCESSFULLY_REACHED){
		wpt_nav_.setInitialPose();
		// Move to recovery position.
		wpt_nav_.moveToWpt(wpt_, false);
	}
	else
	{	if(wpt_nav_.moveToWpt(wpt, true) != WAYPOINT_SUCCESSFULLY_REACHED)
		{
			wpt = WayPointNav::WayPoint<double>(0.201, 4.307, 0.0);
			wpt_nav_.moveToWpt(wpt,false);
		}
		else
		{
			navigation_fsm::docking();
		}
	}
	

	//goal.target_pose.header.frame_id = "map";
	//goal.target_pose.header.stamp = ros::Time::now();
	//goal.target_pose.pose.position.x = -1.04;
	//goal.target_pose.pose.position.y = 0.55;
	//goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(-90*M_PI/180+0.3);
	
	//ac.sendGoal(goal);
	//ac.waitForResult();
	
	//if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
//		ROS_INFO("WAITING FOR DOCKING");
	//else
	//	ROS_INFO("THE ROBOT HAS FAILED TO MOVE");

	///setSlamState(DOCKING);


}	

void navigation_fsm::approachingChargingStationTemp()
{
	float theta;
	WayPointNav::WayPoint<double> wpt_turn;
	std_msgs::String line_follower_str;
	switch(box_in_state_)
	{
		case BoxInState::MOVE_BASE:
			wpt_nav_.setInitialPose();												// Set the pose. Neglect error messages for now

			if(wpt_nav_.moveToWpt(BOX_IN_WPT, true) == WAYPOINT_SUCCESSFULLY_REACHED)
			{
				ROS_ERROR("Waypoint Reached Successfully");
				box_in_state_ = BoxInState::APPROACH_WALL;
			}
			else
			{
				ROS_ERROR("Waypoint not reached. Turning towards the wall.");

				pose_mutex_.lock();
				// Try one more time
				wpt_nav_.moveToWpt(BOX_IN_WPT, true);
				wpt_turn = WayPointNav::WayPoint<double>(pos_.pose.pose.position.x, pos_.pose.pose.position.y, 1.572);
				wpt_nav_.moveToWpt(wpt_turn, true);
				pose_mutex_.unlock();
				box_in_state_ = BoxInState::APPROACH_WALL;

			}
			break;
		case BoxInState::APPROACH_WALL:
			// Slowly move towards the goal
			NavigateBase::moveForward(APPROACH_WALL_VEL);
			if(laser_scanner_dist_ < APPROACH_WALL_DIST)
			{
				ROS_ERROR("TURNING");
				box_in_state_ = BoxInState::TURN;
			}
			else
			{

				box_in_state_ = BoxInState::APPROACH_WALL;
			}
			break;	
		case BoxInState::TURN:
			NavigateBase::turnLeft(TURN_TO_LINE_THETA_VEL);

		    if(line_vel_.twist.linear.z < 40 && line_vel_.twist.linear.z > 0)
		    {
				ROS_ERROR("DOCKING");
				setSlamState(DOCKING);
		    }

			pose_mutex_.lock();
			// Retrieve the theta from the pose
			theta = wpt_nav_.getYawFromQuaternion(pos_);
			ROS_ERROR("Theta is: %f", theta);
			if(theta < TURN_THETA_TRESH && theta > -TURN_THETA_TRESH)
			{
			    line_follower_str.data = "on";
		    	_line_follower.publish(line_follower_str);
			}
			pose_mutex_.unlock();
			break;
		default:
			ROS_ERROR("Error in setting the BoxInState");
		break;
	}
}

void navigation_fsm::docking(){
	MoveBaseClient ac("move_base", true);
	std_msgs::String line_follower_str;
	while(!ac.waitForServer(ros::Duration(5.0))){
	
		ROS_INFO("Waiting for the move_base action server to come up");
  	}

	ROS_INFO("APPROACHING THE CHARGING STATION");
	geometry_msgs::TwistStamped twist;

	//circle equation parameters
	double a = 1.04;
	double b = -1.1;
	double c = 0.2704;
	
	double y_step = 0.11;
	int i = 0;
	double y = 0.55 - y_step*i;
	

	while(i<=5){
		
	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();
	goal.target_pose.pose.position.x = (-a - sqrt(pow(a,2) - 4*(pow(y,2)+b*y+c)))/2;
	goal.target_pose.pose.position.y = y;
	switch(i){
		case 3:

		goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(-90*M_PI/180+0.5);
		ac.sendGoal(goal);
		ac.waitForResult();
		break;
		case 4:
		goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(-90*M_PI/180+0.8);
		ac.sendGoal(goal);
		ac.waitForResult();
		break;
		case 5:
		line_follower_str.data = "on";
       _line_follower.publish(line_follower_str);
		goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(-360*M_PI/180);
		ac.sendGoal(goal);
		ac.waitForResult();
		break;
		default:

		goal.target_pose.pose.orientation= tf::createQuaternionMsgFromYaw(-90*M_PI/180+0.3);
		ac.sendGoal(goal);
		ac.waitForResult();
		break;

	}
	

	//ac.sendGoal(goal, &doneCb, &activeCb, &feedbackCb);
	//ros::spinOnce();
	
	
	i++;	
	
	 y = 0.55 - y_step*i;
	 	
	 	}
		

	setSlamState(DOCKING);	
	ROS_INFO("DONE");
	//
	
}

void navigation_fsm::dockingTemp()
{
	std_msgs::String line_follower_str;
	if(laser_scanner_dist_ > FOLLOW_LINE_WALL_DIST)
	{
		ROS_ERROR("Length os laser_scanner is: %f", laser_scanner_dist_);
		ros::spinOnce();
		ros::Duration(0.1).sleep();
	} 
	else
	{
		line_follower_str.data = "off";
		_line_follower.publish(line_follower_str);
		NavigateBase::stopMovement();
		//pausing the order in case of low battery
		if(NAV_STATE_PREVIOUS == CHARGING_STATION && charging){
			ROS_ERROR("PAUSING ORDER FOR 10 MINUTES");
			ros::Duration(10*60.0).sleep();
			ROS_ERROR("RESUMING ORDER");
			charging = false;
			setSlamState(CHARGING_STATION);
		}else if(NAV_STATE_PREVIOUS == BOX_IN){
		// Announce the idle state to the Mission Exectuion
		ROS_ERROR("Waiting For Orders");
	 	main_state_str_.data = "Idle";
                pub_decider.publish(main_state_str_);
		setSlamState(SLAMstate::IDLE);
		}
	}
}

void navigation_fsm::setSlamState(SLAMstate state){
		
		NAV_STATE_PREVIOUS=NAV_STATE;
		NAV_STATE=state;
}


void navigation_fsm::switchArea(){
		
		std::stringstream ss;
		std_msgs::String msg;
		//change in lego_dispenser
		if(strcmp(robot_state.data.c_str(),"order_received"))
			ss << "SLAM_Navigation";
		else if(strcmp(robot_state.data.c_str(),"SLAM_navigation "))
			ss << "Waypoint_Navigation";
		else if(strcmp(robot_state.data.c_str(),"waypoint_navigation"))
			ss << "SLAM_Navigation";
		else if(NAV_STATE_PREVIOUS == DOCKING)
			ss << "waiting_for_order";
		
		msg.data = ss.str();
		pub_decider.publish(msg);
}	



//topic for orders:order_topic string








