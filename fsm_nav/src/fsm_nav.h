#ifndef _FSM_NAV_H_
#define _FSM_NAV_H_

#include <ros/ros.h>
#include <geometry_msgs/Twist.h> 
#include <geometry_msgs/TwistStamped.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseWithCovariance.h>
#include <geometry_msgs/Pose.h>
#include <msgs/nmea.h>
#include <actionlib/client/simple_action_client.h>
#include <stdlib.h> 
#include <std_msgs/String.h>
#include <std_srvs/Empty.h>
#include <math.h>
#include <cmath>
#include "/home/rsdgroup2/roswork/src/marker_locator/src/waypoint_navigator.hpp"

// DEBUg
#include <mutex>
#include "sensor_msgs/LaserScan.h"
#include "/home/rsdgroup2/roswork/src/marker_locator/src/navigate_base.hpp"

namespace fsm_nav 
{
 
	enum FSMstate
	{		ENABLED,
			DISABLED,
			ROBOT_ERROR
		     };	
	
	enum SLAMstate
	{
			IDLE,
			CHARGING_STATION,
			DISENGAGED,
			BRICK_DISPENSER,
			BOX_OUT,
			LINE_FOLLOWER,
			LINE_FOLLOWER_ACTIVE,
			LINE_FOLLOWER_BACK,
			BOX_IN,
			DOCKING,
			ERROR 		
		};

	enum class BoxInState
	{
		MOVE_BASE,
		APPROACH_WALL,
		TURN
	};

	static const WayPointNav::WayPoint<double> BOX_IN_WPT(-0.774504, 0.190778, -1.572);

	static const float APPROACH_WALL_DIST = 0.18;		// m
	static const float APPROACH_WALL_VEL = 0.1;			// m/s

	static const float TURN_TO_LINE_THETA_VEL = 0.25;	// Radians/s
	static const float TURN_THETA_TRESH = 0.3;			// Radians

	static const float FOLLOW_LINE_WALL_DIST = 0.125;	// m

	static const float DISENGAGED_X_DES
	 = -0.8;
};

using namespace fsm_nav;

class navigation_fsm : public NavigateBase
{

private:
   	ros::NodeHandle nh;
  	ros::Publisher _vel;
	ros::Publisher pub_decider;
    ros::Subscriber _pose;
    ros::Subscriber _sub_decider;
    move_base_msgs::MoveBaseGoal goal;
        ros::ServiceClient clear_costmap_srv_;

    // DEBUG TEMPORARY
    ros::Publisher _line_follower;
    ros::Subscriber _laser_data;
    ros::Subscriber _line_follower_speed;
    ros::Subscriber _battery;

    float laser_scanner_dist_;
    std::mutex mutex_, pose_mutex_;
    BoxInState box_in_state_;
    geometry_msgs::PoseWithCovarianceStamped pos_;
    geometry_msgs::TwistStamped line_vel_;
    bool low_battery;
    bool charging;	
	// Waypoint Navigator
	WptNav<double> wpt_nav_;
	
	std_msgs::String main_state_str_;

	void poseReceivedCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);
	
	void robotStateCallback(const std_msgs::String::ConstPtr& str);	
	
	void batteryCallback(const msgs::nmea::ConstPtr& bat);	

public:
	//constructor
	 navigation_fsm();
	//distructor
	~navigation_fsm(){}

	FSMstate  STATE;
	SLAMstate NAV_STATE;
	SLAMstate NAV_STATE_PREVIOUS;
	std_msgs::String robot_state;

	
	void init();
	
	void fsmWrapper();
	//disengage the robot from the charging station
	void disengaging();

	void navigateToDispenser();
	
	void leaveBox();

	void wptNavigation();

	void wptnavigationBack();

	void approachingChargingStationTemp();
	
	void approachingChargingStation();

	void docking();

	void dockingTemp();
	
	void setSlamState(SLAMstate state);

	void switchArea();
	
	// DEBUG
	void lidarParamReadingCallback(const sensor_msgs::LaserScan::ConstPtr& scan);
	void lineFollowerCallback(const geometry_msgs::TwistStamped::ConstPtr& vel);


};

#endif


