#include "fsm_nav.h"

//NOTE :
//-add waiting for order to enable disable the navigation;
//-add error handling 
// Called once when the goal completes
void doneCb(const actionlib::SimpleClientGoalState& state,
            const move_base_msgs::MoveBaseResult::ConstPtr& result)
{
  ROS_INFO("Finished in state [%s]", state.toString().c_str());
  
  ros::shutdown();
}

// Called once when the goal becomes active
void activeCb()
{
  ROS_INFO("Goal just went active");

}


void feedbackCb(const move_base_msgs::MoveBaseFeedback::ConstPtr& feedback)
{
  
}

void navigation_fsm::lidarParamReadingCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{

	std::lock_guard	<std::mutex> lock(mutex_);
	laser_scanner_dist_ = scan->ranges[125];
}

void navigation_fsm::lineFollowerCallback(const geometry_msgs::TwistStamped::ConstPtr& vel)
{
	if(NAV_STATE == DOCKING)
	{
		geometry_msgs::TwistStamped new_vel;
		new_vel = *vel;

		// Divde speed
		new_vel.twist.linear.x = new_vel.twist.linear.x/2;

		new_vel.header.stamp = ros::Time::now();
		_vel.publish(new_vel);
	} 

}

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
//MoveBaseClient ac("move_base", true);

using namespace fsm_nav;
        
void navigation_fsm::poseReceivedCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg){
		_pos= *msg;
		std::cout <<"\tx:"<<_pos.pose.pose.position.x << "\ty:" << _pos.pose.pose.position.y << "\torientation:" << _pos.pose.pose.orientation.z << std::endl;
}

void navigation_fsm::robotStateCallback(const std_msgs::String::ConstPtr& str)
{
	// Check if the state has changed from line_follower to waypoint
	ROS_ERROR("Recieved string: %s", str->data.c_str());

	if (str->data.compare("Waypoint_Navigation") == 0 && NAV_STATE == SLAMstate::LINE_FOLLOWER_ACTIVE)
	{	
		ROS_ERROR("Change from line to waypoint");
		NAV_STATE = SLAMstate::LINE_FOLLOWER_BACK;
	} 
	// If the robot is in an idle state, start doing something
	else if (str->data.compare("Waypoint_Navigation") == 0 && NAV_STATE != SLAMstate::LINE_FOLLOWER_ACTIVE)
	{
		ROS_ERROR("state changed to charging");
		NAV_STATE = SLAMstate::CHARGING_STATION;//BOX_IN;
	}
	// DEBUG
/*	else if (str->data.compare("Waypoint_Navigation") == 0 && NAV_STATE != SLAMstate::IDLE)
	{
		ROS_ERROR("state changed to LINE_FOLLOWER_BACK");
		NAV_STATE = SLAMstate::LINE_FOLLOWER_BACK;
	}*/
		
		robot_state=*str;
		ROS_ERROR("ROBOT STATE:[%s]",str->data.c_str());
		
}
//class constructor
navigation_fsm::navigation_fsm()
{
//		ros::init(0,"pik", "FSM_MACHINE");
		ROS_WARN("Waypoint fsm started");
		//real vel topic waypoint_slam_navigation_vel_topic
		_vel = nh.advertise<geometry_msgs::TwistStamped>("/waypoint_slam_navigation_vel_topic", 10);
	 	//_vel = nh.advertise<geometry_msgs::TwistStamped>("/fmCommand/cmd_vel",10);
	 	_pose = nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>("/amcl_pose",10,&navigation_fsm::poseReceivedCallback,this);
		pub_decider = nh.advertise<std_msgs::String>("/main_state_topic",10);
		_sub_decider = nh.subscribe<std_msgs::String>("/main_state_topic", 10, &navigation_fsm::robotStateCallback, this);
		
		// DEBUG
		_line_follower = nh.advertise<std_msgs::String>("/line_following_on_off_topic", 10);
		_laser_data = nh.subscribe<sensor_msgs::LaserScan>("/scan_filtered", 10, &navigation_fsm::lidarParamReadingCallback, this);
		_line_follower_speed = nh.subscribe<geometry_msgs::TwistStamped>("/line_follower_topic", 10, &navigation_fsm::lineFollowerCallback, this);

		navigation_fsm::init();
}

void navigation_fsm::init()
{
		
		//temp
		NAV_STATE = SLAMstate::IDLE;// BOX_IN;
		//NAV_STATE = CHARGING_STATION;
		//NAV_STATE = DOCKING;
		//define zones in the map and assign the initial state depending on the localization

}
//wrapper of the state machine
void navigation_fsm::fsmWrapper()
{
	
	//if(strcmp(robot_state.data.c_str(),"order_received") || strcmp(robot_state.data.c_str(),"SLAM_Navigation") || strcmp(robot_state.data.c_str(),"Waypoint_Navigation"))
	//{
		STATE=ENABLED;
	//}
	//else
		//STATE=DISABLED;	
	
	if(STATE == ENABLED)
	{

		switch(NAV_STATE)
		{
			case SLAMstate::IDLE:
			// Await next order.. Callback is used to set the state.
			break;
			case(CHARGING_STATION):
			//switch to order received to slam navigation
			//navigation_fsm::switchArea();
			navigation_fsm::disengaging();
			break;
			case(DISENGAGED):
			navigation_fsm::navigateToDispenser();
			break;
			case(BRICK_DISPENSER):
			navigation_fsm::leaveBox();
			break;
			case(BOX_OUT):
			//switch to wpt navigation state
			//navigation_fsm::switchArea();	
			navigation_fsm::wptNavigation();
			break;
			case(LINE_FOLLOWER):
			main_state_str_.data = "Line_Navigation";
			pub_decider.publish(main_state_str_);
			NAV_STATE = SLAMstate::LINE_FOLLOWER_ACTIVE;
			break;
			case LINE_FOLLOWER_ACTIVE:
			// Do nothing. Go masturbate or something.
			break;
			case(LINE_FOLLOWER_BACK):
			navigation_fsm::wptnavigationBack();
			break;
			case(BOX_IN):
			//switch to slam navigation state
			//navigation_fsm::switchArea();
			navigation_fsm::approachingChargingStationTemp();
			case(DOCKING):
			//switch to waiting for order state
			//navigation_fsm::switchArea();
			navigation_fsm::dockingTemp();
			break;
			case(ERROR):
			//define error solving function
			break;
		}
	}
}
//disengage the robot from the charging station
void navigation_fsm::disengaging(){
	
	ROS_ERROR("DISENGAGING THE CHARGING STATION");
	geometry_msgs::TwistStamped twist;
	double x_des = -0.57;	
	while(_pos.pose.pose.position.x > x_des && ros::ok()){
		
		twist.header.stamp = ros::Time::now();
		twist.twist.linear.x = -0.1;
		
	_vel.publish(twist);
	ros::spinOnce();
	}
	setSlamState(DISENGAGED);
}
//navigate to brick dispenser
void navigation_fsm::navigateToDispenser(){
	MoveBaseClient ac("move_base", true);
	
	while(!ac.waitForServer(ros::Duration(5.0))){
	
		ROS_INFO("Waiting for the move_base action server to come up");
  	}
	
	ROS_ERROR("APPROACHING THE LEGO DISPENSER");
	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();
	goal.target_pose.pose.position.x = -0.70;
	goal.target_pose.pose.position.y = 0.55;
	goal.target_pose.pose.orientation.w = 1.0;
	ac.sendGoal(goal);
	ac.waitForResult();

	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();
	goal.target_pose.pose.position.x = -1.20;
	goal.target_pose.pose.position.y = 0.60;
	goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(-180*M_PI/180);
	ac.sendGoal(goal);
	ac.waitForResult();

	if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
		ROS_INFO("LEGO DISPENSER REACHED");
	else
		ROS_INFO("THE ROBOT HAS FAILED TO MOVE");

	setSlamState(BRICK_DISPENSER);
}
//change goal coordinates with real one
void navigation_fsm::leaveBox(){
	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0))){
	
		ROS_INFO("Waiting for the move_base action server to come up");
  	}
	
	ROS_INFO("MOVING TO THE BOX EXIT");
	
	
	double x_step = 0.09;
	double x_des = -0.778;
	double y_des = 3.518;
	int i = 5;
	double x = x_des - x_step*i;
	double y =( 8.710 + 6.674* x);
	
	while(i >= 0 && ros::ok()){
	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();
	//goal.target_pose.pose.position.x = x;//-0.848;
	//goal.target_pose.pose.position.y = y;//4.3;//3.873;
	goal.target_pose.pose.position.x =-0.848;
	goal.target_pose.pose.position.y = 4.3;
	//goal.target_pose.pose.orientation.z = 0;//0.7679;
	goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(90*M_PI/180);
	//goal.target_pose.pose.orientation = _pos.pose.pose.orientation ;
	//ac.sendGoal(goal, &doneCb, &activeCb, &feedbackCb);
	ac.sendGoal(goal);
	ac.waitForResult();
	i--;
	x = x_des - x_step*i;
	y =(8.710 + 6.674* x);
	ros::spinOnce();
	}

	ROS_INFO("READY TO MOVE OUTSIDE");
	setSlamState(BOX_OUT);
	//setSlamState(BOX_IN);

}

//wptnav to line MARK switch to LINE_FOLLOWER
void navigation_fsm::wptNavigation()
{
	if (wpt_nav_.moveFromBoxToLine() == WayPointNav::WAYPOINT_SUCCESSFULLY_REACHED)
	{
		// Correct position relative to the line
		setSlamState(LINE_FOLLOWER);
	} else
	{
		ROS_ERROR("ERROR FSM_NAV: Unable to reach waypoint");
	}
}
//wptnav from line MARK switch to BOX_IN in the endvoid 
void navigation_fsm::wptnavigationBack() 
{
	if (wpt_nav_.moveFromLineToBox() == WayPointNav::WAYPOINT_SUCCESSFULLY_REACHED)
	{
		// Correct position relative to the line
		setSlamState(BOX_IN);
	} else
	{
		ROS_ERROR("ERROR FSM_NAV: Unable to reach waypoint");
	}
}

void navigation_fsm::approachingChargingStation(){
	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0))){
	
		ROS_INFO("Waiting for the move_base action server to come up");
  	}
	
	ROS_INFO("PREPARING FOR DOCKING");
	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();
	goal.target_pose.pose.position.x = -1.04;
	goal.target_pose.pose.position.y = 0.55;
	goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(-90*M_PI/180+0.3);
	
	ac.sendGoal(goal);
	ac.waitForResult();

	if(ac.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
		ROS_INFO("WAITING FOR DOCKING");
	else
		ROS_INFO("THE ROBOT HAS FAILED TO MOVE");

	setSlamState(DOCKING);


}

void navigation_fsm::approachingChargingStationTemp()
{
	ROS_ERROR("Moving towards docking station");
	wpt_nav_.setInitialPose();
	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0)));
	{
		ROS_INFO("Waiting for the move_base action server to come up");
	}

	// Move to goal-0.842978 0.203773
	WayPointNav::WayPoint<double> wpt(-0.855474, 0.368163, -0.340658);//-0.8587, 0.322146, -0.672953); 
	if(wpt_nav_.moveToWpt(wpt) == WAYPOINT_SUCCESSFULLY_REACHED)
	{
		ROS_ERROR("Waypoint Reached Successfully");
	}
	else
	{
		ROS_ERROR("Waypoint not reached");
	}
	setSlamState(DOCKING);
}

void navigation_fsm::docking(){
	MoveBaseClient ac("move_base", true);

	while(!ac.waitForServer(ros::Duration(5.0))){
	
		ROS_INFO("Waiting for the move_base action server to come up");
  	}

	ROS_INFO("APPROACHING THE CHARGING STATION");
	geometry_msgs::TwistStamped twist;

	//circle equation parameters
	double a = 1.04;
	double b = -1.1;
	double c = 0.2704;
	
	double y_step = 0.11;
	int i = 0;
	double y = 0.55 - y_step*i;
	

	while(i<=6 && ros::ok()){
		
	goal.target_pose.header.frame_id = "map";
	goal.target_pose.header.stamp = ros::Time::now();
	goal.target_pose.pose.position.x = (-a - sqrt(pow(a,2) - 4*(pow(y,2)+b*y+c)))/2;
	goal.target_pose.pose.position.y = y;
	switch(i){
		case 3:
		goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(-90*M_PI/180+0.5);
		ac.sendGoal(goal);
		ac.waitForResult();
		break;
		case 4:
		goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(-90*M_PI/180+0.8);
		ac.sendGoal(goal);
		ac.waitForResult();
		break;
		case 5:
		goal.target_pose.header.frame_id = "map";
		goal.target_pose.header.stamp = ros::Time::now();
		goal.target_pose.pose.position.x = -0.20;
		goal.target_pose.pose.position.y = 0.08;
		goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(-90*M_PI/180+0.9);
				ac.sendGoal(goal);
		ac.waitForResult();
		break;
		case 6:
				goal.target_pose.header.frame_id = "map";
		goal.target_pose.header.stamp = ros::Time::now();
		goal.target_pose.pose.position.x = -0.10;
		goal.target_pose.pose.position.y = 0.08;
		goal.target_pose.pose.orientation = tf::createQuaternionMsgFromYaw(-360*M_PI/180+0.05);
				ac.sendGoal(goal);
				ac.waitForResult();
				break;
		default:
		goal.target_pose.pose.orientation= tf::createQuaternionMsgFromYaw(-90*M_PI/180+0.3);
		ac.sendGoal(goal);
		ac.waitForResult();
		break;

	}
	
	
	//ac.sendGoal(goal, &doneCb, &activeCb, &feedbackCb);
	ros::spinOnce();
	
	
	i++;	
	
	 y = 0.55 - y_step*i;
	 	
	 	}
		

	setSlamState(SLAMstate::IDLE);	
	ROS_INFO("WAITING FOR ORDERS");
	//
	
}

void navigation_fsm::dockingTemp()
{
	ROS_ERROR("Docking now!");
	std_msgs::String line_follower_str;
	line_follower_str.data = "on";
	_line_follower.publish(line_follower_str);
	while(laser_scanner_dist_ > 0.13)
	{
		ROS_ERROR("Length os laser_canner is: %f", laser_scanner_dist_);
		ros::spinOnce();
		ros::Duration(0.1).sleep();
	}
	line_follower_str.data = "off";
	_line_follower.publish(line_follower_str);
	ROS_ERROR("FINISHED!!!!!");
	wpt_nav_.stopMovement();
	setSlamState(SLAMstate::IDLE);

}

void navigation_fsm::setSlamState(SLAMstate state){
		
		NAV_STATE_PREVIOUS=NAV_STATE;
		NAV_STATE=state;
}


void navigation_fsm::switchArea(){
		
		std::stringstream ss;
		std_msgs::String msg;
		//change in lego_dispenser
		if(strcmp(robot_state.data.c_str(),"order_received"))
			ss << "SLAM_Navigation";
		else if(strcmp(robot_state.data.c_str(),"SLAM_navigation "))
			ss << "Waypoint_Navigation";
		else if(strcmp(robot_state.data.c_str(),"waypoint_navigation"))
			ss << "SLAM_Navigation";
		else if(NAV_STATE_PREVIOUS == DOCKING)
			ss << "waiting_for_order";
		
		msg.data = ss.str();
		pub_decider.publish(msg);
}	



//topic for orders:order_topic string








