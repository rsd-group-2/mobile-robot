#include "fsm_nav.h"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "FSM_SLAM");

    navigation_fsm nav;

    ros::Rate loop(10);

    ROS_INFO("Finite state machine started");

    while(ros::ok())
    {
        nav.fsmWrapper();
    	ros::spinOnce();
    	loop.sleep();
    }
    	return 1;
}

