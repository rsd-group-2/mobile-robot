#include <ros/ros.h>
#include <ros/console.h>
#include <geometry_msgs/TwistStamped.h>
#include <sensor_msgs/LaserScan.h>
#include <std_msgs/String.h>
#include <string>
#include <time.h>


//Variables in which the incoming data is saved
bool qr_code_received;
bool tipper_message_received;
bool main_state_message_received;
bool new_order_received;
bool mes_called;
bool mes_tells_tip_up;
bool mes_tells_go_out;
std::string qr_message;
std::string tipper_message;
std::string main_state_message;
std::string received_order;
geometry_msgs::TwistStamped vel_from_line_follower;
geometry_msgs::TwistStamped prev_vel_from_line_follower;
sensor_msgs::LaserScan scanner_output;
std::string ordered_entrance_received;


//Constants
//Phases (for sensors):
#define IDLE 							0
#define ENTERING_ROBOT_ROOM				1
#define APPROACHING_CONVEYOR			2
#define FROM_CONVEYOR_TO_LOADER			3
#define NEAR_LOADER						4
#define FROM_LOADER_TO_HIGHWAY			5
#define OUT_FROM_ROBOT_ROOM				6
//States (for motors):
#define LINE_FOLLOWER					0
#define TURN							1
#define UNLOAD							2
#define TURN_AROUND						3
#define TURN_TO_LOADER					4
#define LOAD							5
#define GO_BACK							6
//Time constants (in seconds)
const double QR_MISSED_THRESH 			= 0.7;
const double TURN_TIME 					= 6.0;
const double TURN_TIME_LEFT				= 5.0;
const double TURN_AROUND_TIME 			= 13.0;
const double UNLOAD_TIME 				= 11.0;
const double LOAD_TIME 					= 11.0;
const double TURN_TO_LOADER_TIME 		= 14.0;
const double GO_BACK_TIME 				= 10.0;

//QR codes constants
const std::string RESTART 				= "GEH8H5";
const std::string LINE_IN 				= "line_in";
const std::string LINE_OUT 				= "line_out";
const std::string CAGE_ONE_ENTRANCE 	= "wc_1_entrance";
const std::string CAGE_TWO_ENTRANCE 	= "wc_2_entrance";
const std::string CAGE_THREE_ENTRANCE 	= "wc_3_entrance";
const std::string CAGE_ONE_EXIT 		= "wc_1_exit";
const std::string CAGE_TWO_EXIT 		= "wc_2_exit";
const std::string CAGE_THREE_EXIT 		= "wc_3_exit";
const std::string CAGE_ONE_CONVEYOR 	= "wc_1_conveyor";
const std::string CAGE_TWO_CONVEYOR 	= "wc_2_conveyor";
const std::string CAGE_THREE_CONVEYOR 	= "wc_3_conveyor";
const std::string CAGE_ONE_LOAD 		= "wc_1_load";
const std::string CAGE_TWO_LOAD 		= "wc_2_load";
const std::string CAGE_THREE_LOAD 		= "wc_3_load";
//Speed constants
const double LINEAR_TURN_CONSTANT 		= 0.11;
const double ANGULAR_TURN_CONSTANT 		= -0.25;
//Laser scanner constants
const int center_angle 					= 135;
const int start_ahead 					= center_angle - 13;
const int end_ahead 					= center_angle + 13;
const int start_ahead_wide	 			= center_angle - 45;
const int end_ahead_wide 				= center_angle + 45;
const int start_right 					= center_angle - 80;
const int end_right 					= center_angle + 15;
const int start_left 					= center_angle -15;
const int end_left 						= center_angle + 80;
const float distance_01m 				= 0.1;
const float distance_02m 				= 0.2;
const float distance_03m 				= 0.3;
const float distance_04m 				= 0.4;
const float distance_07m 				= 0.7;
const float distance_loader				= 0.10;
//Other constants:
#define LEFT 							-1
#define RIGHT 							1
#define AHEAD							0
const int PUBLISHING_FREQUENCY 			= 30;
const int CLEARANCE_THRESH 				= 30;





/*** CALLBACKS ***/

//From MES server
void mesCallback(const std_msgs::String::ConstPtr& msg)
{
	mes_called = true;
	mes_tells_tip_up = false;
	mes_tells_go_out = false;
    if(std::strcmp(msg->data.c_str(), "Tip_up") == 0)
    {
        mes_tells_tip_up = true;
    }
	else if(std::strcmp(msg->data.c_str(), "Go_out") == 0)
    {
        mes_tells_go_out = true;
    }
}

//each time qr code is detected
void qrCallback(const std_msgs::String::ConstPtr& msg)
{
    qr_message = msg->data.c_str();
    qr_code_received = true;
}

//order format: "frobit:_X,_workcell:_X,_bricks:_XX,_red:_XX,_green:_XX,_blue:_XX"
void orderCallback(const std_msgs::String::ConstPtr& msg)
{
    received_order = msg->data.c_str();
	if( received_order[8] == '2' ) //correct frobit
	{
		new_order_received = true;
		if( received_order[21] == '1' )
			ordered_entrance_received = CAGE_ONE_ENTRANCE;
		else if( received_order[21] == '2' )
			ordered_entrance_received = CAGE_TWO_ENTRANCE;
		else if( received_order[21] == '3' )
			ordered_entrance_received = CAGE_THREE_ENTRANCE;
		else
		{
			ROS_ERROR("WRONG ORDER");
			new_order_received = false;
		}
	}
}

//feedback from tipper
void tipperCallback(const std_msgs::String::ConstPtr& msg)
{
    tipper_message = msg->data.c_str();
    tipper_message_received = true;
}

//the node works only when the main state is "Line_Navigation"
void mainStateCallback(const std_msgs::String::ConstPtr& msg)
{
    main_state_message = msg->data.c_str();
    main_state_message_received = true;
}

//line_follower_node transmits the whole set of useful data plus line following velocity
void lineFollowerCallback(const geometry_msgs::TwistStamped::ConstPtr& msg)
{
    vel_from_line_follower = *msg;
}

//laser scanner data is used for obstacle detection
void laserScannerCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
	scanner_output = *scan;
}
/*** END OF CALLBACKS ***/

geometry_msgs::TwistStamped lineFollowing( int velocity_divisor, bool turn_to_conv = false )
{
	geometry_msgs::TwistStamped velocity;
	if( vel_from_line_follower.twist.linear.y < 0.5 )//not a crossing
	{
		velocity.twist.linear.x = vel_from_line_follower.twist.linear.x/(double)velocity_divisor;
		velocity.twist.angular.z = vel_from_line_follower.twist.angular.z/(double)velocity_divisor;
	}
	//in case of crossing, the old data is used (crossing messes line parameters calculations)
	else
	{
		velocity.twist.linear.x = prev_vel_from_line_follower.twist.linear.x/(double)velocity_divisor;
		velocity.twist.angular.z = prev_vel_from_line_follower.twist.angular.z/(double)velocity_divisor;
	}

	if(turn_to_conv)
        {
                velocity.twist.linear.x = vel_from_line_follower.twist.linear.x/3;
                velocity.twist.angular.z = vel_from_line_follower.twist.angular.z*2/3;
        }

	
	return velocity;
}

geometry_msgs::TwistStamped turning( double direction, bool turning_to_cage, int ordered_entrance_int = -1 )
{
	geometry_msgs::TwistStamped velocity;
	
	if( ordered_entrance_int == 2 && turning_to_cage )
		velocity.twist.linear.x = LINEAR_TURN_CONSTANT + 0.01;
	else
		velocity.twist.linear.x = LINEAR_TURN_CONSTANT;

	velocity.twist.angular.z = direction * ANGULAR_TURN_CONSTANT;
	
	return velocity;
}

//obstacle detection works by counting points in the point cloud, that are closer than set distance
bool obstacleDetected( int direction )
{
	int counter = 0;
	int counter2 = 0;
	
	if( scanner_output.ranges.size() < 200 )
		return false;

	switch (direction)
	{
	case AHEAD:
		for (int i = start_ahead_wide; i <= end_ahead_wide; i++)
		{	

			if ( i >= start_ahead && i <= end_ahead && scanner_output.ranges[i] > 0.001 && scanner_output.ranges[i] <= distance_07m) 
			{
				counter++;
			}

			if (scanner_output.ranges[i] > 0.001 && scanner_output.ranges[i] <= distance_03m )
			{
				counter2++;
			}

			if( counter2 > 5 ) counter = 10;
		}
		break;
	case LEFT:
		for (int i = start_left; i <= end_left; i++)
		{	

			if (scanner_output.ranges[i] > 0.001 && scanner_output.ranges[i] <= distance_07m) 
			{
				counter++;
			}
		}
		break;
	case RIGHT:
		for (int i = start_right; i <= end_right; i++)
		{	

			if (scanner_output.ranges[i] > 0.001 && ( ( i > ( center_angle - 55 ) && scanner_output.ranges[i] <= distance_04m ) 
				|| ( i <= ( center_angle - 55 ) && scanner_output.ranges[i] <= distance_07m ) ) ) //( distance_05m*(i-center_angle)/(end_right-center_angle) + distance_02m ) ) 
			{
				counter++;
			}
		}
		break;
	case LOAD:
	{
		for (int i = start_ahead; i <= end_ahead; i++)
		{	

			if (scanner_output.ranges[i] > 0.001 && scanner_output.ranges[i] <= distance_loader) 
			{
				counter++;
			}
		}
	}
	default:
		break;
	}
	
	if( counter > 5 )
		return true;
	else
		return false;

}


int main(int argc, char **argv)
{

    /*** ROS INITIALISATION ***/
	
    //Node initialisation
    ros::init(argc, argv, "line_navigation_executor_node");
    ros::NodeHandle n;
    ROS_INFO("Line navigation executor started");

    //Publishers
    ros::Publisher velocity_pub = n.advertise<geometry_msgs::TwistStamped>("/line_navigation_vel_topic", 10);
    ros::Publisher tipper_pub = n.advertise<std_msgs::String>("/tip_receive", 2);
	ros::Publisher sub_state_pub = n.advertise<std_msgs::String>("/line_navigation_state_topic", 2);
	ros::Publisher main_state_pub = n.advertise<std_msgs::String>("/main_state_topic", 2);
	ros::Publisher mes_pub = n.advertise<std_msgs::String>("/frobit_MES_communication", 2);
	ros::Publisher stop_pub = n.advertise<std_msgs::String>("/stop_line_nodes_topic", 2);



    //Subscriptions
    ros::Subscriber qr_sub = n.subscribe<std_msgs::String>("/qr_scanner_topic", 1, qrCallback);
    ros::Subscriber line_follower_sub = n.subscribe<geometry_msgs::TwistStamped>("/line_follower_topic", 5, lineFollowerCallback);
    ros::Subscriber tipper_sub = n.subscribe<std_msgs::String>("/tip_transmit", 1, tipperCallback);
    ros::Subscriber main_state_sub = n.subscribe<std_msgs::String>("/main_state_topic", 1, mainStateCallback);
	ros::Subscriber order_sub = n.subscribe<std_msgs::String>("/order_topic", 1, orderCallback);
	ros::Subscriber laser_scanner_sub = n.subscribe<sensor_msgs::LaserScan>("/scan", 1, laserScannerCallback);
	ros::Subscriber mes_sub = n.subscribe<std_msgs::String>("/MES_frobit_communication", 1, mesCallback);


    //Parameters
    int frequency;
    n.param("frequency", frequency, PUBLISHING_FREQUENCY);
    ros::Rate loop_rate(frequency);
    ros::Rate idle_rate(1);

    /*** END OF ROS INITIALISATION ***/

    //Variables declaration
    geometry_msgs::TwistStamped vel_msg;
    std_msgs::String to_tipper_msg;
	std_msgs::String string_msg;
	std::string ordered_entrance;
    int phase;
    int time;
	int velocity_divisor_for_qr;
	int velocity_divisor_for_speed_in_cage;
	int velocity_divisor;
    double start_turn;
    double start_unload;
    double start_load;
    double start_turn_to_loader;
    double start_turn_around;
    double start_go_back;
    double pause_time;
    double direction;
	double position_error;
	double proportion_of_black_on_picture;
	double angular_error;
	double time_correction;
	double start_after_qr;
	double start_cage_3_tuning;
	double start_seek_for_line;
	double start_wait_time;
	double start_closer_conveyor;
	int swing_i;
	bool swing_up_and_down;
    bool entrance_recognised;
    bool time_started;
	bool line_out_recognised;
	bool exit_to_highway_recognised;
	bool conveyor_recognised;
	bool loading_area_recognised;
	bool crossing_detected;
	bool straightened;
	bool go_back;
	bool turn_to_highway;
	bool turn_around;
	bool from_turn_around_to_exit_qr;
	bool turn_from_conveyor;
	bool go_forward_a_little;
	bool turn_to_loader;
	bool pause_started;
	bool time_started_after_qr;
	bool line_not_found;
	bool time_init_started;
	bool time_started_wait;
	bool time_started_closer_conveyor;


    //Variables initialisation
    vel_from_line_follower.twist.linear.x 		= 0;
    vel_from_line_follower.twist.linear.y 		= 0;
    vel_from_line_follower.twist.linear.z 		= 0;
    vel_from_line_follower.twist.angular.z		= 0;
    vel_from_line_follower.twist.angular.y 		= 0;
    vel_from_line_follower.twist.angular.z	 	= 0;
    prev_vel_from_line_follower.twist.linear.x 	= 0;
    prev_vel_from_line_follower.twist.angular.z = 0;
	position_error 								= 0;
    qr_code_received 							= false;
    tipper_message_received 					= false;
    main_state_message_received 				= false;
    entrance_recognised 						= false;
	new_order_received 							= false;
	line_out_recognised 						= false;
	exit_to_highway_recognised 					= false;
    conveyor_recognised 						= false;
    loading_area_recognised 					= false;
    crossing_detected 							= false;
	straightened 								= false;
	go_back 									= false;
	turn_to_highway 							= false;
	turn_around 								= false;
	turn_from_conveyor 							= false;
	go_forward_a_little 						= false;
	turn_to_loader 								= false;
	from_turn_around_to_exit_qr 				= false;
	pause_started 								= false;
	time_started_after_qr						= false;
	line_not_found 								= false;
	time_init_started 							= false;
	time_started_wait							= false;
	time_started_closer_conveyor 				= false;
	mes_called									= false;
	mes_tells_tip_up							= false;
	mes_tells_go_out							= false;
	swing_up_and_down 							= false;
	swing_i									= 0;
	start_closer_conveyor 						= 0;
	start_wait_time 							= 0;
	start_after_qr								= 0;
    time 										= 0;
    start_turn 									= 0;
    start_unload 								= 0;
    start_load 									= 0;
    start_turn_to_loader 						= 0;
    direction 									= 0;
    start_go_back 								= 0;
	start_cage_3_tuning							= 0;
	start_seek_for_line							= 0;
    time_started 								= false;
    ordered_entrance 							= CAGE_TWO_ENTRANCE;
	velocity_divisor 							= 1;
    velocity_divisor_for_speed_in_cage 			= 1;
	velocity_divisor_for_qr 					= 1;
	phase										= IDLE;
	    
    //To get info from subscribers
    ros::spinOnce();

    while (ros::ok())
    {
		 
        //robot switched to the new state
        if( main_state_message_received )
	    {
	        main_state_message_received = false;
	        if( main_state_message == "Line_Navigation" )
	        {
                phase = ENTERING_ROBOT_ROOM;
				string_msg.data = "ENTERING_ROBOT_ROOM";
				sub_state_pub.publish( string_msg );
				line_not_found = true;
			time_init_started = false;
			time_started = false;
			position_error = 1000;
	       
		 }
	        else
	        {
                phase = IDLE;
				string_msg.data = "IDLE";
				sub_state_pub.publish( string_msg );
	        }
	    }
	
	    //Robot is in different state (not line navigation)
	    if( phase == IDLE )
	    {
	    	if( new_order_received )
			{
				new_order_received = false;
				ordered_entrance = ordered_entrance_received;
			}
	        ros::spinOnce();
            idle_rate.sleep();
	        continue; 
	    }
		
		/**
		 * Various data about parameters calculated from camera is received
		 * from line_follower_node
		 */
		//data about the line
		position_error = vel_from_line_follower.twist.linear.z;
		angular_error = vel_from_line_follower.twist.angular.x;
		//data about the whole picture
		proportion_of_black_on_picture = vel_from_line_follower.twist.linear.y;
		//qr-like object on sight
		velocity_divisor_for_qr = vel_from_line_follower.twist.angular.y;

        //new qr code detected
        if( qr_code_received )
        {
            qr_code_received = false;
            exit_to_highway_recognised = false;
            conveyor_recognised = false;
            loading_area_recognised = false;
		line_out_recognised = false;
            if( qr_message == RESTART ) //Additional code for restarting line following
            {
                phase = ENTERING_ROBOT_ROOM;
				string_msg.data = "ENTERING_ROBOT_ROOM";
				sub_state_pub.publish( string_msg );
            }
            else if( qr_message == ordered_entrance )
            {
                entrance_recognised = true;
            }
			else if( qr_message == LINE_OUT )
			{
				line_out_recognised = true;
			}
			else if( qr_message == CAGE_ONE_EXIT
				|| qr_message == CAGE_TWO_EXIT || qr_message == CAGE_THREE_EXIT )
			{
				exit_to_highway_recognised = true;
			}
			else if( qr_message == CAGE_ONE_CONVEYOR
				|| qr_message == CAGE_TWO_CONVEYOR || qr_message == CAGE_THREE_CONVEYOR )
			{
				conveyor_recognised = true;
			}
			else if( qr_message == CAGE_ONE_LOAD
				|| qr_message == CAGE_TWO_LOAD || qr_message == CAGE_THREE_LOAD )
			{
				loading_area_recognised = true;
			}
        }

		//when the qr-like object is on sight or the robot is inside of the cage, it slows down
		velocity_divisor = std::max( velocity_divisor_for_qr, velocity_divisor_for_speed_in_cage );

		/*** "phase" STATE MACHINE START ***/
        switch ( phase )
        {
	    case ENTERING_ROBOT_ROOM: //frome line_in until the robot is in the correct workcell
			/*** VELOCITY ***/
	    	//it is checked, if the line is visible at the begining
	    	if( line_not_found )
	    	{
	    						//the entrance has not been detected, so the order can still be changed; later it's impossible
				if( new_order_received )
				{
					new_order_received = false;
					ordered_entrance = ordered_entrance_received;
				}

	    		crossing_detected = false;
	    		time_started = false;

	    		if( !time_init_started )
	    		{
	    			start_seek_for_line = ros::Time::now().toSec();
	    			time_init_started = true;
	    		}
	    		else if( ( ros::Time::now().toSec() - start_seek_for_line ) < 1.0 )
	    		{
	    			vel_msg.twist.linear.x = 0;
					vel_msg.twist.angular.z = 0;

	    		}
	    		else if( ( ros::Time::now().toSec() - start_seek_for_line ) < 10.0 )
	    		{
	    			vel_msg.twist.linear.x = 0;
					vel_msg.twist.angular.z = 0.15;

	    		}
	    		else if( ( ( ros::Time::now().toSec() - start_seek_for_line ) < 30.0 ) )
	    		{
	    			vel_msg.twist.linear.x = 0;
					vel_msg.twist.angular.z = -0.15;
	    		}
	    		else
	    		{
	    			ROS_ERROR("LINE NOT FOUND, LINE NAVIGATION GOES TO IDLE");
	    			phase = IDLE;
					string_msg.data = "IDLE";
					sub_state_pub.publish( string_msg );
					vel_msg.twist.linear.x = 0;
					vel_msg.twist.angular.z = 0;
					time_init_started = false;
	    		}

	    		if( position_error < 70 && position_error > 0 )
	    		{
	    			line_not_found = false;
	    			time_init_started = false;
	    		}
	    	}
			//before the entrance is detected
			else if( !time_started )
			{
				if( !time_init_started )
				{
					start_seek_for_line = ros::Time::now().toSec();
					time_init_started = true;
				}
				//the entrance has not been detected, so the order can still be changed; later it's impossible
				if( new_order_received )
				{
					new_order_received = false;
					ordered_entrance = ordered_entrance_received;
				}

				//obstacle detection
				if( obstacleDetected( AHEAD ) )
				{
					vel_msg.twist.linear.x = 0;
					vel_msg.twist.angular.z = 0;
				}
				else if( ( ros::Time::now().toSec() - start_seek_for_line ) < 30.0 )
					vel_msg = lineFollowing( 1 );
				else
					vel_msg = lineFollowing( velocity_divisor );
			}
			//after the correct qr code is detected, but before the line crossing
			else if( !crossing_detected )
			{
				if( proportion_of_black_on_picture > 0.4 )
					crossing_detected = true;
				else
					vel_msg = lineFollowing( velocity_divisor );
			}
			//turning starts at the crossing after the correct qr code
			else
			{
				if( ordered_entrance == CAGE_TWO_ENTRANCE || ordered_entrance == CAGE_ONE_ENTRANCE )
					vel_msg = turning( RIGHT, true, 2 );
				else
					vel_msg = turning( RIGHT, true );
			}


	        /*** CONTROL ***/
	        //The turn to the recognised cage is started
            if( entrance_recognised && !time_started)
            {
                start_turn = ros::Time::now().toSec();
				time_started = true;
				velocity_divisor_for_speed_in_cage = 2;
            }
			
			//third workcell needs longer turn time
			if( ordered_entrance == CAGE_THREE_ENTRANCE )
				time_correction = 1.0;
			else if( ordered_entrance == CAGE_ONE_ENTRANCE )
				time_correction = 0.4;
			else
				time_correction = 1.1;
			
			//the turn, so ENTERING_ROBOT_ROOM phase as well, is finished
            if( time_started && ( ( ros::Time::now().toSec() - start_turn ) > TURN_TIME + time_correction ) 
            	&& ( ( ordered_entrance == CAGE_ONE_ENTRANCE && position_error < 50 ) || ( ordered_entrance != CAGE_ONE_ENTRANCE && position_error < 140 ) ) )
            {
				//the phase is changed
                phase = APPROACHING_CONVEYOR;
				
				//information to HMI
				string_msg.data = "APPROACHING_CONVEYOR";
				sub_state_pub.publish( string_msg );
				
				//relevant flags are cleared
                time_started = false;
                entrance_recognised = false;
				crossing_detected = false;
				time_started_closer_conveyor = false;
				time_init_started = false;
			line_not_found = true;
            }

	        break;
		case APPROACHING_CONVEYOR: //from the workcell entrance until conveyor belt
			/*** VELOCITY ***/

			if( !crossing_detected )
			{
				//until it detects black area under conveyor belt
				if( proportion_of_black_on_picture > 0.6 && !time_started_closer_conveyor )
				{
					time_started_closer_conveyor = true;
					start_closer_conveyor = ros::Time::now().toSec();
				}
				if( !entrance_recognised )
				{
					vel_msg.twist.linear.x = 0;
					vel_msg.twist.angular.z = 0;
					entrance_recognised = true;
				}
				else
					vel_msg = lineFollowing( velocity_divisor, true );

			}
			else
			{
				vel_msg.twist.linear.x = 0;
				vel_msg.twist.angular.z = 0;
			}
			
			/*** CONTROL ***/
			if( time_started_closer_conveyor && ( ros::Time::now().toSec() - start_closer_conveyor ) > 0.8 )
			{
				string_msg.data = "I_arrived_to_conveyor";
                                mes_pub.publish(string_msg);

				time_started_closer_conveyor = false;
				crossing_detected = true;
			}

		    if( crossing_detected && !time_started )
			{
				if( mes_called && mes_tells_tip_up )
				{
					mes_called = false;
					start_unload = ros::Time::now().toSec();
					time_started = true;
					to_tipper_msg.data = "1";
					tipper_pub.publish( to_tipper_msg );
				}
            }

			//for testing
			/*if( time_started && ( ros::Time::now().toSec() - start_unload ) > UNLOAD_TIME )
			{
				//the phase is changed
				phase = FROM_CONVEYOR_TO_LOADER;
				
				//information for HMI
				string_msg.data = "FROM_CONVEYOR_TO_LOADER";
				sub_state_pub.publish( string_msg );
				
				//relevant flags are cleared
				time_started = false;
				crossing_detected = false;
				
				//first subphase in the FROM_CONVEYOR_TO_LOADER phase is triggered
				turn_from_conveyor = true;
			}*/


			//when the tipper is ready
			//Tipper control
			if( !swing_up_and_down && time_started && tipper_message_received && tipper_message == "1" && to_tipper_msg.data == "1" && ( ros::Time::now().toSec() - start_unload ) > 6.0 )
			{
				//ros::Duration(2.0).sleep();
				tipper_message_received = false;
				to_tipper_msg.data = "2";
				tipper_pub.publish( to_tipper_msg );
				swing_up_and_down = true;
				swing_i = 0;
			}

			if( swing_up_and_down )
			{
				if( swing_i == 3)
					swing_up_and_down = false;
				if( tipper_message_received && tipper_message == "1" && to_tipper_msg.data == "1" )
                        	{

                                	tipper_message_received = false;
                                	to_tipper_msg.data = "2";
                                	tipper_pub.publish( to_tipper_msg );
					swing_i++;

       				}
                                if( tipper_message_received && tipper_message == "2" && to_tipper_msg.data == "2" )
                                {

                                        tipper_message_received = false;
                                        to_tipper_msg.data = "1";
                                        tipper_pub.publish( to_tipper_msg );
                                }
			}



			if( !swing_up_and_down && time_started && tipper_message_received && tipper_message == "2" && to_tipper_msg.data == "2" )
			{
				phase = FROM_CONVEYOR_TO_LOADER;
				string_msg.data = "FROM_CONVEYOR_TO_LOADER";
				sub_state_pub.publish( string_msg );
				//relevant flags are cleared
				time_started = false;
				crossing_detected = false;
				entrance_recognised = false;
				
				//first subphase in the FROM_CONVEYOR_TO_LOADER phase is triggered
				turn_from_conveyor = true;
			}
	  
			break;

		case FROM_CONVEYOR_TO_LOADER: //the name says it all :)
			/*** VELOCITY ***/
			//note that turn_from_conveyor, go_forward_a_little and turn_to_loader flags are altered in the CONTROL part below
			
			//turn_from_conveyor is by default true, so here it starts - it just turns until relevant event (see below)
			if( turn_from_conveyor )
			{
				vel_msg.twist.linear.x = 0;
				
				//conveyor belt is not in the same side, so the direction of the turn is different
				if( ordered_entrance == CAGE_TWO_ENTRANCE )
					vel_msg.twist.angular.z = LEFT * ANGULAR_TURN_CONSTANT;
				else
					vel_msg.twist.angular.z = RIGHT * ANGULAR_TURN_CONSTANT;
			}
			//after the turn is finished, it goes forward until it sees black area under the table in workcell
			else if( go_forward_a_little )
			{
				vel_msg.twist.linear.x = 0.05;
				vel_msg.twist.angular.z = 0;
			}
			//then it turns to be at the centre of the line leading to loading area...
			else if( turn_to_loader )
			{
				vel_msg.twist.linear.x = 0;
				
				//again, the direction of the turn depends on the workcell
				if( ordered_entrance == CAGE_TWO_ENTRANCE )
					vel_msg.twist.angular.z = RIGHT * ANGULAR_TURN_CONSTANT;
				else
					vel_msg.twist.angular.z = LEFT * ANGULAR_TURN_CONSTANT;
			}
			else
			{
				vel_msg = lineFollowing( velocity_divisor );
			}

			
			/*** CONTROL ***/
			
			//the timer is started as soon as this phase is entered
			if( !time_started )
			{
				start_turn_to_loader = ros::Time::now().toSec();
				time_started = true;
			}
			

			/**
			 * Each workcell is slightly different, this is why this condition seems to be complicated.
			 * Explanation: if the timer is started and the robot is still in the first subphase (it turns standing near conveyor belt),
			 * and turning time exceeds 5 seconds, then:
			 * 1. In the first workcell - turn until there is less than 0.03 black in the picture
			 * (which means the robot turned so much, that the line leading to loader is not visible any more);
			 * 2. In the second workcell - turn until the line leading to loader is seen by the angle between 60 and 67 or is not seen at all;
			 * 3. In the third workcell - turn until the line leading to loader is seen by the angle between -67 and -55 or is not seen at all.
			 * Remember - when something seems to be stupid, but works - it's not stupid :) 
			 */
			if( time_started && turn_from_conveyor && ( ros::Time::now().toSec() - start_turn_to_loader ) > 5.0 
				&& ( ( ( ordered_entrance == CAGE_ONE_ENTRANCE && proportion_of_black_on_picture < 0.03 ) 
				|| ( ordered_entrance == CAGE_TWO_ENTRANCE && ( ( angular_error > 60 ) || proportion_of_black_on_picture < 0.04 ) )
				|| ( ordered_entrance == CAGE_THREE_ENTRANCE && ( ( angular_error < -55 ) || proportion_of_black_on_picture < 0.04 ) ) )
				|| ( ( ros::Time::now().toSec() - start_turn_to_loader ) > 6.0 ) ) )
			{
				//those flags switch the subphases above in the VELOCITY part of this phase
				turn_from_conveyor = false;
				go_forward_a_little = true;
			}

			/**
			 * The go_forward_a_little subphase finishes when the robot sees the black area under the table.
			 * Because there is the KUKA equipment low in the third cage, this phase has to finish a little earlier there.
			 * Afterwards, it starts turning to align with the line leading to the loader 
			 */
			if( go_forward_a_little && ( ( ordered_entrance == CAGE_THREE_ENTRANCE && proportion_of_black_on_picture > 0.6 )
				|| ( ordered_entrance != CAGE_THREE_ENTRANCE && proportion_of_black_on_picture > 0.9) ) )
			{
				go_forward_a_little = false;
				turn_to_loader = true;
				start_turn_to_loader = ros::Time::now().toSec();
			}

			//tuning
			if( ordered_entrance == CAGE_ONE_ENTRANCE )
				time_correction = 0.5;
			else
				time_correction = 0.4;

			//the turning (and this phase) is finished after certain amount of time passes and the line is in the centre (low position_error)
			if( turn_to_loader && ( ros::Time::now().toSec() - start_turn_to_loader ) > ( 4.0 + time_correction ) && position_error < 40 )
			{
				//the phase is changed
				phase = NEAR_LOADER;
				
				//information for HMI
				string_msg.data = "NEAR_LOADER";
				sub_state_pub.publish( string_msg );
				
				//flags are cleared
				time_started = false;
				crossing_detected = false;
				turn_to_loader = false;
				time_started_after_qr = false;
				time_started_wait = false;
			}
			break;

		case NEAR_LOADER: //this phase starts when the robot stands on the line leading to loader and ends when loading is over
			/*** VELOCITY ***/
			//time_started is false at the beginning, so this subphase is started as soon as the phase is entered
			if( !time_started_wait )
			{
				time_started_wait = true;
				start_wait_time = ros::Time::now().toSec();
				vel_msg.twist.linear.x = 0;
				vel_msg.twist.angular.z = 0;
			}
			else if( time_started_wait && ( ros::Time::now().toSec() - start_wait_time ) < 0.2 )
			{
				vel_msg.twist.linear.x = 0;
				vel_msg.twist.angular.z = 0;
			}
			else if( !time_started )
			{
				/* The robot follows the line leading to loader until it sees the black KUKA robot base,
				   or when it didn't see the black base 1.5 seconds after the loader QR code has been read
				   (sometimes when it's bright, the light reflection off the base makes it harder to recognise) */
				//if( ( time_started_after_qr && ( ( ros::Time::now().toSec() - start_after_qr ) > 3.5 ) ) || obstacleDetected( LOAD ) )
					// ) proportion_of_black_on_picture > 0.7
				if( obstacleDetected( LOAD ) )
				{
					crossing_detected = true;
                        	        string_msg.data = "Load_me";
                	                mes_pub.publish(string_msg);

        	                        string_msg.data = "off";
	                                stop_pub.publish(string_msg);
				}


				vel_msg = lineFollowing( velocity_divisor );
			}
			else
			{
				vel_msg.twist.linear.x = 0;
				vel_msg.twist.angular.z = 0;
			}
			
			/*** CONTROL ***/
			if( loading_area_recognised )
			{
				loading_area_recognised = false;
				time_started_after_qr = true;
				start_after_qr = ros::Time::now().toSec();
			}
			
			if( crossing_detected && !time_started )
			{
				start_load = ros::Time::now().toSec();
				time_started = true;
			}

			//It waits for the constant time until the loading should be finished
			if( time_started && mes_called && mes_tells_go_out )//( ros::Time::now().toSec() - start_load ) > LOAD_TIME )
			{
				string_msg.data = "on";
				stop_pub.publish(string_msg);

				//the phase is changed
				phase = FROM_LOADER_TO_HIGHWAY;
				
				//info for HMI
				string_msg.data = "FROM_LOADER_TO_HIGHWAY";
				sub_state_pub.publish( string_msg );
				
				//flags are cleared
				time_started_after_qr = false;
				crossing_detected = false;
				time_started = false;
				straightened = false;
				turn_around = false;
				turn_to_highway = false;
				pause_started = false;
				
				//first subphase of the next phase is set
				go_back = true;
			}
	  
			break;

		case FROM_LOADER_TO_HIGHWAY: //this phase starts when the loading is over and continues when the robot is on highway
			/*** VELOCITY ***/
			//this subphase is only one in the whole line navigation, that is triggered only by time
			if( go_back )
			{
				vel_msg.twist.linear.x = -LINEAR_TURN_CONSTANT/2;
				vel_msg.twist.angular.z = 0;
				
				//a little tuning for the third cage
				/*if( ordered_entrance == CAGE_THREE_ENTRANCE && time_started 
					&& ( ros::Time::now().toSec() - start_go_back ) < 1.5 )
					vel_msg.twist.angular.z = LEFT * 0.1;*/
			}
			//name says it all
			else if( turn_around )
			{
				vel_msg.twist.linear.x = 0;
				
				//again, direction of turn around depends on workcell
				if( ordered_entrance == CAGE_TWO_ENTRANCE )
					vel_msg.twist.angular.z = RIGHT * ANGULAR_TURN_CONSTANT;
				else
					vel_msg.twist.angular.z = LEFT * ANGULAR_TURN_CONSTANT;
			}
			//name says it all
			else if( from_turn_around_to_exit_qr )
			{
				/* After turning around, the robot checks if there are no other robots going on the main line and lets them go if there are.
				   There is a little tuning in the third workcell, because it sees the table for the first 2.5 seconds. */
				if( obstacleDetected( LEFT ) && !( ordered_entrance == CAGE_THREE_ENTRANCE && ( ros::Time::now().toSec() - start_cage_3_tuning ) < 2.5 ) )
				{
					vel_msg.twist.linear.x = 0;
					vel_msg.twist.angular.z = 0;
				}
				else //no obstacles
					vel_msg = lineFollowing( velocity_divisor );
			}
			/**
			 * After the QR code workcell exit has been read, but before the crossing has been detected, again it is checked 
			 * if there are any robots on the main lines (this time in the right side) 
			 */
			else if( turn_to_highway && !crossing_detected )
			{
				if( obstacleDetected( RIGHT ) )
				{
					vel_msg.twist.linear.x = 0;
					vel_msg.twist.angular.z = 0;
				}
				else //no obstacles
				{
					//if crossing is detected
					if( proportion_of_black_on_picture > 0.4 )
					{
						crossing_detected = true;
						start_turn = ros::Time::now().toSec();
					}

					vel_msg = lineFollowing( velocity_divisor );
				}
			}
			//during the actual turn to the main line it's too late to check for the robots anyway
			else if( turn_to_highway && crossing_detected )
			{
				vel_msg = turning( LEFT, false );
			}
			else //this should not happen
			{
				vel_msg.twist.linear.x = 0;
				vel_msg.twist.angular.z = 0;
			}

			
			/*** CONTROL ***/
			//as soon as this phase is started - the time is needed for going back
			if( !time_started )
			{
				start_go_back = ros::Time::now().toSec();
				time_started = true;
			}

			//tuning
			if( ordered_entrance == CAGE_ONE_ENTRANCE )
				time_correction = 4.0;
			else if( ordered_entrance == CAGE_THREE_ENTRANCE )
				time_correction = 1.0;
			else
				time_correction = 0.0;
			
			//when going back time passes
			if( go_back && time_started && ( ros::Time::now().toSec() - start_go_back ) > ( GO_BACK_TIME + time_correction ) )
			{
				go_back = false;
				turn_around = true;
				//the timer for turning around starts
				start_turn_around = ros::Time::now().toSec();
			}

			//when the time for turning around passes and the line is in the centre of the view
			if( turn_around && time_started 
				&& ( ros::Time::now().toSec() - start_turn_around ) > TURN_AROUND_TIME && position_error < 40 )
			{
				turn_around = false;
				from_turn_around_to_exit_qr = true;
				
				//cage 3 has special needs (see above)
				start_cage_3_tuning = ros::Time::now().toSec();
			}
			
			//the key condition here is "exit_to_highway_recognised", the rest is just to make sure it happens in correct subphase
			if( exit_to_highway_recognised && time_started && !turn_around && !go_back && from_turn_around_to_exit_qr )
			{
				turn_to_highway = true;
				from_turn_around_to_exit_qr = false;
			}

			//the turn to main line is finished
			if( time_started && turn_to_highway && position_error < 40 && crossing_detected && ( ros::Time::now().toSec() - start_turn ) > TURN_TIME_LEFT )
			{
				//change phase
				phase = OUT_FROM_ROBOT_ROOM;
				
				//info for HMI
				string_msg.data = "OUT_FROM_ROBOT_ROOM";
				sub_state_pub.publish( string_msg );
				
				//flags are cleared
				time_started = false;
				turn_to_highway = false;
				crossing_detected = false;
			}
			break;

		case OUT_FROM_ROBOT_ROOM: //going along the main line to the exit

			if( !time_started )
			{
				time_started = true;
				start_after_qr = ros::Time::now().toSec();
			}

			if( new_order_received )
			{
				new_order_received = false;
				ordered_entrance = ordered_entrance_received;
			}
		
			//back to normal velocity (not slowed down)
			velocity_divisor_for_speed_in_cage = 1;
			
			//obstacle detection
			if( obstacleDetected( AHEAD ) )
			{
				vel_msg.twist.linear.x = 0;
				vel_msg.twist.angular.z = 0;
			}
			else if( ( ros::Time::now().toSec() - start_after_qr ) < 55.0 ) //no obstacles
				vel_msg = lineFollowing( 1 );
			else
				vel_msg = lineFollowing( velocity_divisor );
		
			//QR code line_out recognised - Line_Navigation is over
			if( line_out_recognised )
			{
				//change main state, disabling this node
				string_msg.data = "Waypoint_Navigation";
				main_state_pub.publish( string_msg );
				vel_msg.twist.linear.x = 0;
				vel_msg.twist.angular.z = 0;
				line_out_recognised = false;
			}
			break;
		default:
			ROS_ERROR("UNRECOGNISED PHASE");
			break;
		}
		/*** END OF "phase" STATE MACHINE ***/
	
		//this assured that corrupted data from line following during crossings is not used
		if( proportion_of_black_on_picture < 0.5 )
		{
			prev_vel_from_line_follower = vel_from_line_follower;
		}


		//Velocity is published to the robot with current time stamp
		vel_msg.header.stamp = ros::Time::now();
		velocity_pub.publish(vel_msg);
		ros::spinOnce();

		loop_rate.sleep();

	}


	return 0;
}
