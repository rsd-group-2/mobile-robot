#include <ros/ros.h>
#include <ros/console.h>
#include "std_msgs/String.h"
#include <sstream>
#include <termios.h>

int getch()
{
  static struct termios oldt, newt;
  tcgetattr( STDIN_FILENO, &oldt);           // save old settings
  newt = oldt;
  newt.c_lflag &= ~(ICANON);                 // disable buffering      
  tcsetattr( STDIN_FILENO, TCSANOW, &newt);  // apply new settings

  int c = getchar();  // read character (non-blocking)

  tcsetattr( STDIN_FILENO, TCSANOW, &oldt);  // restore old settings
  return c;
}

/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{

  ros::init(argc, argv, "emergency_stop_node");

  ros::NodeHandle n;

  ROS_INFO("Emergency stop node started");

  ros::Publisher pub = n.advertise<std_msgs::String>("/emergency_stop_topic", 1);

  //ros::Rate loop_rate(10);

  std::stringstream ss;
  std_msgs::String msg;
  int c;
  ROS_INFO("Press 's' to stop, 'r' to run,");
  ROS_INFO("'u' to go straight for set amount of time,");
  ROS_INFO("'e' to go straight without obstacle detection");
  ROS_INFO("and 'd' to go back to normal");

  while (ros::ok())
  {

    c = getch();    

    if(c == 's')
    {
      ROS_INFO("Stop");
      ss << "stop";
      msg.data = ss.str();
      pub.publish(msg);
      ss.str(std::string());
    }
    else if(c == 'r')
    {
      ROS_INFO("Run");
      ss << "run";
      msg.data = ss.str();
      pub.publish(msg);
      ss.str(std::string());
    }
    else if(c == 'u')
    {
      ROS_INFO("Go up the ramp");
      ss << "up_ramp";
      msg.data = ss.str();
      pub.publish(msg);
      ss.str(std::string());
    }
    else if(c == 'e')
    {
      ROS_INFO("Going straight started");
      ROS_INFO("WARNING: OBSTACLE DETECTION STOP DISABLED");
      ss << "go_straight_start";
      msg.data = ss.str();
      pub.publish(msg);
      ss.str(std::string());
    }

    else if(c == 'd')
    {
      ROS_INFO("Going straight stopped");
      ROS_INFO("Emergency stop active, press 'r' to run again");
      ss << "stop_going_straight";
      msg.data = ss.str();
      pub.publish(msg);
      ss.str(std::string());
    }


    ros::spinOnce();

    //loop_rate.sleep();
  }
  return 0;
}
